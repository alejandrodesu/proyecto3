package model.logic;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.concurrent.ThreadLocalRandom;

public class GeneradorDatos 
{
	private SecureRandom random = new SecureRandom();
	
	public static String[] generarCadenasPrueba(int N)
	{
		String [] arreglo = new String [N];
		char a = 'a';
		for (int i = 0; i < N; i++)
		{
			String aniadir = "";
			for (int j = 0; j < i+1; j++)
				aniadir += a;
			
			arreglo[i]= aniadir;
		}
		return arreglo;
	}
	
	public static String[] generarCadenas(int N, int k)
	{
		String [] arreglo = new String [N];
		char a = 'a';
		for (int i = 0; i < arreglo.length; i++)
		{
			String aniadir = "";
			for (int j = 0; j < ThreadLocalRandom.current().nextInt(1, k+1); j++)
				aniadir += a;
			
			arreglo[i]= aniadir;
		}
		return arreglo;
	}
	
	public static Integer[] generarNumeros(int N)
	{
		Integer [] arreglo = new Integer [N];
		for (int i = 0; i < arreglo.length; i++)
			arreglo[i] = ThreadLocalRandom.current().nextInt(0, 999999999);
		return arreglo;
	}
	
	public String[] generarCadenas(int N)
	{
		String [] arreglo = new String [N];
		for (int i = 0; i < arreglo.length; i++)
			arreglo[i] = new BigInteger(130, random).toString(32);
		return arreglo;
	}
	
	public static Integer[] generarAgnos(int N)
	{
		Integer [] arreglo = new Integer [N];
		for (int i = 0; i < arreglo.length; i++)
			arreglo[i] = ThreadLocalRandom.current().nextInt(1950, 2018);
		return arreglo;
	}
}
