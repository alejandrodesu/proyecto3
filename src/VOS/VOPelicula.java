package VOS;

import java.util.NoSuchElementException;

import API.ILista;
import data_structures.EncadenamientoSeparadoTH;
import data_structures.ListaEnlazadaSimple;
import data_structures.RedBlackBST;
import VOS.VORating;

public class VOPelicula {

	private int id;
	private String titulo;
	private String[] generos;
	private ListaEnlazadaSimple<VORating> ratings;
	private EncadenamientoSeparadoTH <Integer, Double> tablaPeliculas;
	private RedBlackBST<Long, ListaEnlazadaSimple <VOPeliculaPlan>> arbolFunciones [];

	public VOPelicula(int id, String titulo, String[] generos) {
		this.id = id;
		this.titulo = titulo;
		this.generos = generos;
		ratings = new ListaEnlazadaSimple<>();
		tablaPeliculas = new EncadenamientoSeparadoTH<>(25);
		arbolFunciones = (RedBlackBST<Long, ListaEnlazadaSimple <VOPeliculaPlan>> []) new RedBlackBST [6];
		for (int i = 0; i < arbolFunciones.length; i++ )
			arbolFunciones[i] = new RedBlackBST<Long, ListaEnlazadaSimple <VOPeliculaPlan>> ();
	}
	public double getSimilitud (int idPelicula){
		Double simi = tablaPeliculas.darValor(idPelicula);
		if (simi == null)
			return 0.0;
		return simi;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String[] getGeneros() {
		return generos;
	}
	public void setGeneros(String[] generos) {
		this.generos = generos;
	}
	public ILista <VORating> getRatings(){
		return ratings;
	}
	public void agregarRating (VORating rating){
		ratings.agregarElementoFinal(rating);
	}
	public void agregarSimilitud (int idSimi, Double simi){
		tablaPeliculas.insertar(idSimi, simi);
	}
	public EncadenamientoSeparadoTH<Integer, Double> getTablaSimilitudes(){
		return tablaPeliculas;
	}
	public void agregarFuncion (VOPeliculaPlan funcion){
		ListaEnlazadaSimple <VOPeliculaPlan> lista = null;
		try{
			lista = arbolFunciones[funcion.getDia()].get(funcion.getHoraInicio().getTime());
		}catch (NoSuchElementException e){
			lista = new ListaEnlazadaSimple<VOPeliculaPlan>();
			arbolFunciones[funcion.getDia()].put(funcion.getHoraInicio().getTime(), lista);
		}
		lista.agregarElementoFinal(funcion);
	}
	public VOPeliculaPlan getPrimeraFuncion (int dia){
		return arbolFunciones[dia].minValue().verPrimero();
	}
	
	public ListaEnlazadaSimple <VOPeliculaPlan> darPeliculasSiguienteFuncion(int dia, long siguiente)
	{
		ListaEnlazadaSimple <VOPeliculaPlan> funciones = new ListaEnlazadaSimple<VOPeliculaPlan>();
		
		for (ListaEnlazadaSimple <VOPeliculaPlan> planes : arbolFunciones[dia].inorden(true))
		{
			for (VOPeliculaPlan plan : planes)
			{
				if (plan.getHoraInicio().getTime() >= siguiente)
				{
					funciones.agregarElementoFinal(plan);
				}
			}
		}
		
		return funciones;
		
	}
	
	public boolean tieneGenero(String genero)
	{
		boolean condicion = false;
		
		for (int i = 0; i < generos.length && !condicion; i++)
		{
			if (generos[i].equals(genero))
			{
				condicion = true;
			}
		}
	
		return condicion;
	}
}
