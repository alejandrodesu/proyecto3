package VOS;


/**
 * @author Venegas
 *
 */

public class VOUsuarioPelicula implements Comparable <VOUsuarioPelicula>{

	/**
	 * nombre de la pel�cula
	 */

	private String nombrepelicula;
	
	private int idPelicula;
	/**
	 * rating dado por el usuario
	 */
	private double ratingUsuario;

	/**
	 * rating calculado por el sistema
	 */

	private double ratingSistema;

	/**
	 * error sobre el rating
	 */

	private double errorRating;

	/**
	 * id del usuario
	 */

	private int idUsuario;

	private boolean calculado = false;

	public VOUsuarioPelicula (){
		
	}
	public VOUsuarioPelicula (int idUsuario, int idPelicula, String nombrePelicula){
		setIdUsuario(idUsuario);
		setNombrepelicula(nombrePelicula);
		setIdPelicula(idPelicula);
	}

	public String getNombrepelicula() {
		return nombrepelicula;
	}

	public void setNombrepelicula(String nombrepelicula) {
		this.nombrepelicula = nombrepelicula;
	}

	public double getRatingUsuario() {
		return ratingUsuario;
	}

	public void setRatingUsuario(double ratingUsuario) {
		this.ratingUsuario = ratingUsuario;
	}

	public double getRatingSistema() {
		return ratingSistema;
	}

	public void setRatingSistema(double ratingSistema) {
		this.ratingSistema = ratingSistema;
		if (ratingUsuario != 0.0)
			calcularError();
	}

	public void calcularError() {
		if (ratingSistema != -1)
			this.errorRating = Math.abs(ratingSistema-ratingUsuario);
		else
			this.errorRating = -1;
		this.calculado = true;
	}

	public double getErrorRating() {
		return errorRating;
	}

	public void setErrorRating(double errorRating) {
		this.errorRating = errorRating;
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public boolean esCalculado (){
		return calculado;
	}

	@Override
	public int compareTo(VOUsuarioPelicula o) {
		if (ratingSistema == o.ratingSistema)
			return 0;
		else
			return (ratingSistema < o.ratingSistema)? -1:1 ;
	}

	public int getIdPelicula () {
		return idPelicula;
	}
	public void setIdPelicula (int idPelicula){
		this.idPelicula = idPelicula;
	}

	public String toString (){
		return "" + idUsuario + "/Peli:" + idPelicula + "/Pred:" + ratingSistema;
	}
}
