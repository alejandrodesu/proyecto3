package VOS;

public class VORating {
	
	private int idUsuario;
	private int idPelicula;
	private double rating;
	
	public VORating (int idUsuario, int idPelicula, double rating){
		this.idPelicula = idPelicula;
		this.idUsuario = idUsuario;
		this.rating = rating;
	}
	
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public int getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(int idPelicula) {
		this.idPelicula = idPelicula;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}

}
