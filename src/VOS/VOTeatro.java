
package VOS;

import java.sql.Time;
import java.util.Date;
import java.util.NoSuchElementException;

import data_structures.ListaEnlazadaSimple;
import data_structures.RedBlackBST;

/**
 *  
 * @Author: Tomas F. Venegas 
 */
public class VOTeatro implements Comparable <VOTeatro>{

	/**
	 * Atributo que modela el nombre del teatro
	 */

	private String nombre;

	/**
	 * Atributo que modela la ubicacion del teatro 
	 */

	private VOUbicacion ubicacion;

	/**
	 * Atributo que referencia la franquicia
	 */

	private VOFranquicia franquicia;
	private RedBlackBST<Long, ListaEnlazadaSimple <VOPeliculaPlan>> arbolFunciones [];

	public VOTeatro() {
	}
	public VOTeatro(String nombre, VOUbicacion ubicacion, VOFranquicia franquicia) {
		setNombre(nombre);
		setFranquicia(franquicia);
		setUbicacion(ubicacion);
		arbolFunciones = (RedBlackBST<Long, ListaEnlazadaSimple <VOPeliculaPlan>> []) new RedBlackBST [6];
		for (int i = 0; i < arbolFunciones.length ; i++)
			arbolFunciones[i] = new RedBlackBST <Long, ListaEnlazadaSimple <VOPeliculaPlan>> ();
	}

	public String getNombre(){
		return nombre;
	}
	public void setNombre(String nombre){
		this.nombre = nombre;
	}
	public VOUbicacion getUbicacion (){
		return ubicacion;
	}
	public void setUbicacion (VOUbicacion ubicacion){
		this.ubicacion = ubicacion;
	}
	public VOFranquicia getFranquicia (){
		return franquicia;
	}
	public void setFranquicia(VOFranquicia franquicia){
		this.franquicia = franquicia;
	}
	public Iterable <VOPeliculaPlan> getFunciones (Time tiempoMin, int dia){
		ListaEnlazadaSimple <VOPeliculaPlan> lista = new ListaEnlazadaSimple<>();
		for (ListaEnlazadaSimple <VOPeliculaPlan> listaSimple: arbolFunciones[dia].darLlavesValoresEnRango(tiempoMin.getTime(), arbolFunciones[dia].max()))
			lista = lista.unirLista(listaSimple);
		return lista;
	}
	public void agregarFuncion(VOPeliculaPlan funcion){
		ListaEnlazadaSimple <VOPeliculaPlan> lista = null;
		try{
			lista = arbolFunciones[funcion.getDia()].get(funcion.getHoraInicio().getTime());
		}
		catch (NoSuchElementException e){
			lista = new ListaEnlazadaSimple<VOPeliculaPlan>();
			arbolFunciones[funcion.getDia()].put(funcion.getHoraInicio().getTime(), lista);
		}
		lista.agregarElementoFinal(funcion);
	}
	public int hashCode (){
		return nombre.hashCode();
	}
	@Override
	public int compareTo(VOTeatro arg0) {
		if (nombre.equals(arg0.nombre))
			return 0;
		return (nombre.compareTo(arg0.nombre) <0)? -1:1;
	}
	public String toString (){
		return nombre + " - Franquicia:" + franquicia.toString();
	}
}
