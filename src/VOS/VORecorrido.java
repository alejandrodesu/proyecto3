package VOS;

import java.sql.Time;

import data_structures.ListaEnlazadaSimple;

public class VORecorrido implements Comparable <VORecorrido>{
	public Time tiempoActual;
	public ListaEnlazadaSimple<VOPeliculaPlan> listaFunciones;
	public double sumRecomendacion;
	public VOTeatro teatro;
	
	public VORecorrido (Time tiempoActual, double sumRecomendacion, ListaEnlazadaSimple<VOPeliculaPlan> listaFunciones, VOTeatro teatro){
		this.tiempoActual = tiempoActual;
		this.sumRecomendacion = sumRecomendacion;
		this.listaFunciones = listaFunciones;
		this.teatro = teatro;
	}
	public VORecorrido (Time tiempoActual){
		this.tiempoActual = tiempoActual;
		this.listaFunciones = new ListaEnlazadaSimple<>();
	}
	public VORecorrido (){
		this.listaFunciones = new ListaEnlazadaSimple<>();
	}
	public int numFunciones (){
		return listaFunciones.darNumeroElementos();
	}
	public void agregarFuncion (VOPeliculaPlan peliPlan){
		listaFunciones.agregarElementoFinal(peliPlan);
	}
	private ListaEnlazadaSimple<VOPeliculaPlan> darListaFunciones (VOPeliculaPlan peliPlan){
		ListaEnlazadaSimple<VOPeliculaPlan> lista = listaFunciones.darPrimerosElementos(listaFunciones.darNumeroElementos());
		lista.agregarElementoFinal(peliPlan);
		return lista;
	}
	public VORecorrido darNuevoRecorrido(double recomendacion, int minutos, VOPeliculaPlan peliPlan, VOTeatro teatroP){
		return new VORecorrido (peliPlan.getHoraFin(), sumRecomendacion+recomendacion, darListaFunciones(peliPlan), teatroP);
	}
	
	@Override
	public int compareTo(VORecorrido o) {
		if (numFunciones() == o.numFunciones()){
			if (sumRecomendacion == o.sumRecomendacion)
				{return 0;}
			return (sumRecomendacion < o.sumRecomendacion)? -1:1;
		}
		return (numFunciones() < o.numFunciones()) ? -1:1;
	}
}

