package VOS;
/**
 *  
 * @Author: Tomas F. Venegas 
 */

public class VOFranquicia {
	
	/**
	 * Atributo que modela el nombre de la franquicia
	 */
	private String nombre;
	
	public VOFranquicia() {
	}
	public VOFranquicia(String nombreP) {
		setNombre(nombreP);
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String toString (){
		return nombre;
	}
}
