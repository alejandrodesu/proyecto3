package VOS;

import API.ILista;
import data_structures.EncadenamientoSeparadoTH;
import data_structures.ListaEnlazadaSimple;


/**
 *  
 * @Author: Tomas F. Venegas 
 */
public class VOUsuario {

	/**
	 * Atributo que modela el id del usuario
	 */

	private long id;
	private ListaEnlazadaSimple <VORating> ratings;
	private EncadenamientoSeparadoTH <Integer, VOUsuarioPelicula> usuariosPeliculas;

	public VOUsuario() {
		ratings = new ListaEnlazadaSimple<>();
		usuariosPeliculas = new EncadenamientoSeparadoTH<>(30);
	}
	public VOUsuario(long id) {
		setId(id);
		ratings = new ListaEnlazadaSimple<>();
		usuariosPeliculas = new EncadenamientoSeparadoTH<>(30);
	}

	public void setId(long id){
		this.id = id;
	}
	public long getId(){
		return id;
	}
	public VOUsuarioPelicula agregarRating (VORating rating, String pelicula){
		ratings.agregarElementoFinal(rating);

		VOUsuarioPelicula usuarioPelicula = usuariosPeliculas.darValor(rating.getIdPelicula());
		if (usuarioPelicula == null){
			usuarioPelicula = new VOUsuarioPelicula(rating.getIdUsuario(), rating.getIdPelicula(), pelicula);
			usuariosPeliculas.insertar(rating.getIdPelicula(), usuarioPelicula);
		}

		usuarioPelicula.setRatingUsuario(rating.getRating());
		return usuarioPelicula;
	}

	public ILista <VORating> darRatings(){
		return ratings;
	}
	public EncadenamientoSeparadoTH <Integer, VOUsuarioPelicula> darUsuariosPelicula (){
		return usuariosPeliculas;
	}
	
	public int compararPredicciones(VOPelicula peli1, VOPelicula peli2){
		VOUsuarioPelicula usuPeli1 = usuariosPeliculas.darValor(peli1.getId());
		VOUsuarioPelicula usuPeli2 = usuariosPeliculas.darValor(peli2.getId());
		if (usuPeli1.getRatingSistema() == usuPeli2.getRatingSistema())
			return 0;
		return (usuPeli1.getRatingSistema() < usuPeli2.getRatingSistema()) ? -1:1;
	}
	public double darRecomendacion (VOPelicula peli1){
		VOUsuarioPelicula usuPeli1 = usuariosPeliculas.darValor(peli1.getId());
		if (usuPeli1 == null)
			return 0.0;
		return usuPeli1.getRatingSistema();
	}
	public void setRatingSistema(VOPelicula pelicula, double prediccion){
		VOUsuarioPelicula usuPeli = usuariosPeliculas.darValor(pelicula.getId());
		if (usuPeli == null){
			usuPeli = new VOUsuarioPelicula((int)id, pelicula.getId(), pelicula.getTitulo());
			usuariosPeliculas.insertar(pelicula.getId(), usuPeli);
		}
		usuPeli.setRatingSistema(prediccion);
	}
}
