package API;

public class NodoCamino<K> {
	private K from;
	private K to;
	private boolean mark;
	private double peso;
	private char ordenL;
	
	public NodoCamino(K Ffrom,K Tto) {
		// TODO Auto-generated constructor stub
		from = Ffrom;
		to=Tto;
		peso=0;
		ordenL='0';
	}
	public NodoCamino(K Ffrom,K Tto,char pOrden,double pPeso) {
		// TODO Auto-generated constructor stub
		from = Ffrom;
		to=Tto;
		ordenL=pOrden;
		peso=pPeso;
		
	}
	public K darOrigen(){
		return from;
		
	}
	public K darLlegada(){
		return to;
	}
	public double darPeso(){
		return peso;
	}
}
