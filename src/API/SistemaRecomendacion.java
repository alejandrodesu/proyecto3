package API;

import java.io.BufferedReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.csvreader.CsvReader;

import VOS.VOFranquicia;
import VOS.VOGeneroPelicula;
import VOS.VOPelicula;
import VOS.VOPeliculaPlan;
import VOS.VORating;
import VOS.VOTeatro;
import VOS.VOUbicacion;
import VOS.VOUsuario;
import VOS.VOUsuarioPelicula;
import data_structures.EncadenamientoSeparadoTH;
import data_structures.ListaEnlazadaSimple;
import data_structures.WeightedGraph;

public class SistemaRecomendacion implements ISistemaRecomendacion {
	private WeightedGraph <VOTeatro, VOTeatro> grafo;
	private EncadenamientoSeparadoTH <Integer, VOPelicula> tablaPeliculas;
	private EncadenamientoSeparadoTH <Integer, VOUsuario> tablaUsuarios;
	private EncadenamientoSeparadoTH <String, VOTeatro> tablaTeatros;
	private EncadenamientoSeparadoTH<Integer, VOPeliculaPlan> tablaDias;
	private Time ultimaFuncion;

	public SistemaRecomendacion (){
		grafo =  new WeightedGraph <VOTeatro, VOTeatro> (100); 
		tablaPeliculas = new EncadenamientoSeparadoTH<> (100);
		tablaUsuarios = new EncadenamientoSeparadoTH<> (100);
		tablaTeatros = new EncadenamientoSeparadoTH<>(100);
	}
	@Override
	public ISistemaRecomendacion crearSR() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean cargarTeatros(String ruta) {
		// TODO Auto-generated method stub
		BufferedReader br;
		try {
			br = new BufferedReader (new FileReader (new File (ruta)));
			String lista = br.readLine();
			String actual = null;
			while (true){
				actual = br.readLine();
				if (actual == null)
					break;
				lista = lista + actual;
			}
			lista = lista.substring(3, lista.length());
			JSONArray array = new JSONArray(lista);
			for (Object obj : array){
				JSONObject object =  (JSONObject) obj;
				String nombre = object.getString("Nombre");
				String ubicacionLatLong = object.getString("UbicacionGeografica(Lat|Long)");
				String [] arregloLatLong = {ubicacionLatLong.substring(0, ubicacionLatLong.indexOf("|")),   ubicacionLatLong.substring(ubicacionLatLong.indexOf("|")+1)};
				VOUbicacion ubicacion = null;
				try{
					double lat = Float.parseFloat(arregloLatLong[0].trim());
					double longi = Float.parseFloat(arregloLatLong[1].trim());
					ubicacion = new VOUbicacion(lat, longi);
				}
				catch (NumberFormatException e){
					e.printStackTrace();
				}
				String franquicia = object.getString("Franquicia");
				VOFranquicia voFanquicia = new VOFranquicia(franquicia);
				VOTeatro teatro = new VOTeatro(nombre, ubicacion, voFanquicia);
				grafo.agregarVertice(teatro, teatro);
				tablaTeatros.insertar(nombre, teatro);
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}


	public boolean cargarCartelera (String ruta){
		return true;
	}
	public boolean cargarCartelera(String ruta1, String ruta2,String ruta3, String ruta4,String ruta5) {
		Time ultimaFuncion = null;
		Time ultimaFuncion2 = null;
		String [] arregloRutas = {ruta1, ruta2, ruta3, ruta4, ruta5};
		int indice = 1;
		SimpleDateFormat format = new SimpleDateFormat("HH:mm");
		BufferedReader br;
		try {
			for (String ruta : arregloRutas){
				br = new BufferedReader (new FileReader (new File (ruta)));
				String lista = br.readLine();
				String actual = null;
				while (true){
					actual = br.readLine();
					if (actual == null)
						break;
					lista = lista + actual;	 
				}
				JSONArray array = new JSONArray(lista);
				for (Object obj : array){
					JSONObject object =  (JSONObject) obj;
					String teatroNombre = object.getString("Teatro");
					JSONArray peliculas = object.getJSONArray("Pelicula");
					for (Object objPeli : peliculas){
						JSONObject pelicula =  (JSONObject) objPeli;
						int id = pelicula.getInt("Id");
						JSONArray funciones = pelicula.getJSONArray("Funciones");
						for (Object objFuncion : funciones){
							JSONObject funcion =  (JSONObject) objFuncion;
							String horaString = funcion.getString("Hora");
							VOPelicula voPelicula = tablaPeliculas.darValor(id);
							VOTeatro voTeatro = tablaTeatros.darValor(teatroNombre);
							try {
								Time timeInicio = new Time(format.parse(horaString).getTime());
								int hora = (Integer.parseInt(horaString.substring(0, horaString.indexOf(":"))))+2;
								horaString = hora + horaString.substring(horaString.indexOf(":"));
								Time timeFin = new Time(format.parse(horaString).getTime());
								VOPeliculaPlan peliPlan = new VOPeliculaPlan(voPelicula, voTeatro, timeInicio, timeFin, indice);
								voPelicula.agregarFuncion(peliPlan);
								voTeatro.agregarFuncion(peliPlan);
								if (ultimaFuncion == null || ultimaFuncion.getTime() < timeFin.getTime()){
									ultimaFuncion = timeFin;
									ultimaFuncion2 = timeInicio;
								}
							} catch (ParseException e) {
								e.printStackTrace();
							}
						}
					}
				}
				br.close();
				indice++;
			} 
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		System.out.println("�ltima Funci�n:" + ultimaFuncion + "&&" +ultimaFuncion2);
		this.ultimaFuncion = ultimaFuncion2;
		return true;
	}

	@Override
	public boolean cargarRed(String ruta) {
		// TODO Auto-generated method stub
		BufferedReader br;
		try {
			br = new BufferedReader (new FileReader (new File (ruta)));
			String lista = br.readLine();
			String actual = null;
			while (true){
				actual = br.readLine();
				if (actual == null)
					break;
				lista = lista + actual;	 
			}
			JSONArray array = new JSONArray(lista);
			for (Object obj : array)
			{
				JSONObject object =  (JSONObject) obj;
				String teatro1 = object.getString("Teatro 1").trim();
				String teatro2 = object.getString("Teatro 2").trim();
				double minutos = object.getDouble("Tiempo (minutos)");
				grafo.agregarArco(tablaTeatros.darValor(teatro1), tablaTeatros.darValor(teatro2), minutos);
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean cargarPeliculas (String ruta){
		BufferedReader br;
		try {
			br = new BufferedReader (new FileReader (new File (ruta)));
			String lista = br.readLine();
			String actual = null;
			while (true){
				actual = br.readLine();
				if (actual == null)
					break;
				lista = lista + actual;

			}
			JSONArray array = new JSONArray(lista);
			for (Object obj : array)
			{
				JSONObject object =  (JSONObject) obj;
				int id = object.getInt("movie_id");
				String nombre = object.getString("title");
				String generosString = object.getString("genres");
				String [] generosArreglo = new String [1];
				if (!generosString.contains("|"))
					generosArreglo[0] = generosString.trim();
				else{
					char [] charArreglo = generosString.toCharArray();
					ListaEnlazadaSimple<String> listaGeneros = new ListaEnlazadaSimple<>();
					String gene = "";
					for (char c : charArreglo){
						if (c == '|'){
							listaGeneros.agregarElementoFinal(gene.trim());
							gene = "";
							continue;
						}
						gene += c;
					}
					generosArreglo = new String [listaGeneros.darNumeroElementos()];
					int i = 0;
					for (String s : listaGeneros){
						generosArreglo[i] = s;
						i++;
					}
				}
				VOPelicula peli = new VOPelicula(id, nombre, generosArreglo);
				tablaPeliculas.insertar(id, peli);
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean cargarRatingsSR(String rutaRaitings) {
		try {
			CsvReader peliculas_import = new CsvReader(rutaRaitings);
			peliculas_import.readHeaders();

			while (peliculas_import.readRecord())
			{
				int idUsuario = Integer.parseInt(peliculas_import.get(0));
				int idPelicula = Integer.parseInt(peliculas_import.get(1));
				double ratingDouble = Double.parseDouble(peliculas_import.get(2));
				VORating rating = new VORating (idUsuario, idPelicula,ratingDouble);

				VOPelicula pelicula = tablaPeliculas.darValor(idPelicula);
				pelicula.agregarRating(rating);
				VOUsuario usuario = tablaUsuarios.darValor(idUsuario);
				if (usuario == null){
					usuario = new VOUsuario(idUsuario);
					tablaUsuarios.insertar(idUsuario, usuario);
				}
				usuario.agregarRating(rating, pelicula.getTitulo());	
			}
			peliculas_import.close();

		} catch (FileNotFoundException e) {
			return false;
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	public boolean cargarSimilitudes (String ruta){
		BufferedReader br;
		try {
			br = new BufferedReader (new FileReader (new File (ruta)));
			String lista = br.readLine();
			String actual = null;
			while (true){
				actual = br.readLine();
				if (actual == null)
					break;
				lista = lista + actual;	 
			}
			JSONArray array = new JSONArray(lista);
			for (Object obj : array)
			{
				JSONObject object =  (JSONObject) obj;
				int id = object.getInt("movieId");
				VOPelicula pelicula = tablaPeliculas.darValor(id);
				for (VOPelicula peliculaSimi : tablaPeliculas){
					try	{
						Double simi = object.getDouble(""+ peliculaSimi.getId());
						pelicula.agregarSimilitud(peliculaSimi.getId(), simi);
					}
					catch (JSONException e){
						assert (object.getString(""+ peliculaSimi.getId()).equals("NaN"));
					}
				}
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	
		return true;
	}
	
	

	@Override
	public int sizeMovies() {
	
		return 		tablaPeliculas.darTamanio();
	}

	@Override
	public int sizeTeatros() {
		return  tablaTeatros.darTamanio();
	}

	
	@Override
	public ILista<VOPeliculaPlan> PlanPeliculas(VOUsuario usuario, int fecha) {
		
		return null;
	}

	

	@Override
	public ILista<VOPeliculaPlan> PlanPorGenero(VOGeneroPelicula genero, VOUsuario usuario) {
		// TODO Auto-generated method stub
		//Declaro mi lista para retornar
		ListaEnlazadaSimple< VOPeliculaPlan> lista = new ListaEnlazadaSimple<VOPeliculaPlan>();
		//Declaracion de las listas, cada lista tiene un plan de pelicula por dia
		ListaEnlazadaSimple<VOPeliculaPlan> dia1 = new ListaEnlazadaSimple<VOPeliculaPlan>();
		ListaEnlazadaSimple<VOPeliculaPlan> dia2 = new ListaEnlazadaSimple<VOPeliculaPlan>();
		ListaEnlazadaSimple<VOPeliculaPlan> dia3 = new ListaEnlazadaSimple<VOPeliculaPlan>();
		ListaEnlazadaSimple<VOPeliculaPlan> dia4 = new ListaEnlazadaSimple<VOPeliculaPlan>();
		ListaEnlazadaSimple<VOPeliculaPlan> dia5 = new ListaEnlazadaSimple<VOPeliculaPlan>();
		//A cada lista le asigno el plan de pelicula por dia
		dia1 = (ListaEnlazadaSimple<VOPeliculaPlan>) PlanPeliculas(usuario, 1);
		dia2 = (ListaEnlazadaSimple<VOPeliculaPlan>) PlanPeliculas(usuario, 2);
		dia3 = (ListaEnlazadaSimple<VOPeliculaPlan>) PlanPeliculas(usuario, 3);
		dia4 = (ListaEnlazadaSimple<VOPeliculaPlan>) PlanPeliculas(usuario, 4);
		dia5 = (ListaEnlazadaSimple<VOPeliculaPlan>) PlanPeliculas(usuario, 5);
		//Recorro dia 1 y miro sus generos
		ListaEnlazadaSimple<VOGeneroPelicula> generos1 = new ListaEnlazadaSimple<VOGeneroPelicula>();
		for(int i = 0; i < dia1.darNumeroElementos(); i++)
		{
			VOGeneroPelicula genero1 = new VOGeneroPelicula();
			for(int j = 0; j<dia1.darElemento(i).getPelicula().getGeneros().length; j++)
			{
				genero1.setNombre(dia1.darElemento(i).getPelicula().getGeneros()[j]);
			}
			generos1.agregarElementoFinal(genero1);
		}
		//Ahora recorro mi lista de generos y miro las ocurrencias con el genero por parametro
		int ocurrencias1 = 0;
		for(int i = 0; i < generos1.darNumeroElementos(); i++)
		{
			if(generos1.darElemento(i).equals(genero))
			{
				ocurrencias1++;
			}
		}
		//Recorro dia 2 y miro sus generos
		ListaEnlazadaSimple<VOGeneroPelicula> generos2 = new ListaEnlazadaSimple<VOGeneroPelicula>();
		for(int i = 0; i < dia2.darNumeroElementos(); i++)
		{
			VOGeneroPelicula genero2 = new VOGeneroPelicula();
			for(int j = 0; j<dia2.darElemento(i).getPelicula().getGeneros().length; j++)
			{
				genero2.setNombre(dia1.darElemento(i).getPelicula().getGeneros()[j]);
			}
			generos1.agregarElementoFinal(genero2);
		}
		//Ahora recorro mi lista de generos y miro las ocurrencias con el genero por parametro
		int ocurrencias2 = 0;
		for(int i = 0; i < generos2.darNumeroElementos(); i++)
		{
			if(generos2.darElemento(i).equals(genero))
			{
				ocurrencias2++;
			}
		}
		//Recorro dia 3 y miro sus generos
		ListaEnlazadaSimple<VOGeneroPelicula> generos3 = new ListaEnlazadaSimple<VOGeneroPelicula>();
		for(int i = 0; i < dia3.darNumeroElementos(); i++)
		{
			VOGeneroPelicula genero3 = new VOGeneroPelicula();
			for(int j = 0; j<dia3.darElemento(i).getPelicula().getGeneros().length; j++)
			{
				genero3.setNombre(dia3.darElemento(i).getPelicula().getGeneros()[j]);
			}
			generos3.agregarElementoFinal(genero3);
		}
		//Ahora recorro mi lista de generos y miro las ocurrencias con el genero por parametro
		int ocurrencias3 = 0;
		for(int i = 0; i < generos3.darNumeroElementos(); i++)
		{
			if(generos3.darElemento(i).equals(genero))
			{
				ocurrencias3++;
			}
		}
		//Recorro dia 4 y miro sus generos
		ListaEnlazadaSimple<VOGeneroPelicula> generos4 = new ListaEnlazadaSimple<VOGeneroPelicula>();
		for(int i = 0; i < dia4.darNumeroElementos(); i++)
		{
			VOGeneroPelicula genero4 = new VOGeneroPelicula();
			for(int j = 0; j<dia4.darElemento(i).getPelicula().getGeneros().length; j++)
			{
				genero4.setNombre(dia4.darElemento(i).getPelicula().getGeneros()[j]);
			}
			generos4.agregarElementoFinal(genero4);
		}
		//Ahora recorro mi lista de generos y miro las ocurrencias con el genero por parametro
		int ocurrencias4 = 0;
		for(int i = 0; i < generos1.darNumeroElementos(); i++)
		{
			if(generos1.darElemento(i).equals(genero))
			{
				ocurrencias4++;
			}
		}
		//Recorro dia 5 y miro sus generos
		ListaEnlazadaSimple<VOGeneroPelicula> generos5 = new ListaEnlazadaSimple<VOGeneroPelicula>();
		for(int i = 0; i < dia5.darNumeroElementos(); i++)
		{
			VOGeneroPelicula genero5 = new VOGeneroPelicula();
			for(int j = 0; j<dia5.darElemento(i).getPelicula().getGeneros().length; j++)
			{
				genero5.setNombre(dia5.darElemento(i).getPelicula().getGeneros()[j]);
			}
			generos1.agregarElementoFinal(genero5);
		}
		//Ahora recorro mi lista de generos y miro las ocurrencias con el genero por parametro
		int ocurrencias5 = 0;
		for(int i = 0; i < generos5.darNumeroElementos(); i++)
		{
			if(generos5.darElemento(i).equals(genero))
			{
				ocurrencias5++;
			}
		}
		//Busco el plan con mas ocurrencias del genero pedido
		String masOcurrencias = "";
		int masOcurrente = ocurrencias5;
		ListaEnlazadaSimple<Integer> misOcurrencias = new ListaEnlazadaSimple<Integer>();
		misOcurrencias.agregarElementoFinal(ocurrencias1);
		misOcurrencias.agregarElementoFinal(ocurrencias2);
		misOcurrencias.agregarElementoFinal(ocurrencias3);
		misOcurrencias.agregarElementoFinal(ocurrencias4);
		misOcurrencias.agregarElementoFinal(ocurrencias5);
		for(int i = 0; i<misOcurrencias.darNumeroElementos(); i++)
		{
			if(masOcurrente<misOcurrencias.darElemento(i)){
				masOcurrente=misOcurrencias.darElemento(i);
				masOcurrencias=""+i;
			}
		}
		//Decido cual es la lista que debo retornar
		if(masOcurrencias.equals("0"))
		{
			lista = dia1;
		}
		else if(masOcurrencias.equals("1")){
			lista = dia2;
		}
		else if(masOcurrencias.equals("2")){
			lista = dia3;
		}
		else if(masOcurrencias.equals("3")){
			lista = dia4;
		}
		else if(masOcurrencias.equals("4")){
			lista = dia5;
		}
		return lista;
	}

	@Override
	public ILista<VOPeliculaPlan> PlanPorFranquicia(VOFranquicia franquicia, int fecha, String franja) {
		ListaEnlazadaSimple< VOPeliculaPlan> lista = new ListaEnlazadaSimple<VOPeliculaPlan>();
		VOTeatro teatroActual =null;
		long horaActual = 0;
		long dia1 = 10L;
		long dia2 = 12L;
		long tarde1 = 12L;
		long tarde2 = 18L;
		//Recorro mis peliculas
		if(franja.equals(""))
		{
			for(int i = 0; i<tablaPeliculas.darTamanio(); i++)
			{
				VOPelicula mejorpelicula = tablaPeliculas.darValor(i); 
				ListaEnlazadaSimple <VOPeliculaPlan> funcionSiguiente = mejorpelicula.darPeliculasSiguienteFuncion(fecha, horaActual);
				for(VOPeliculaPlan plan : funcionSiguiente)
				{
					if (lista.darNumeroElementos() == 0)
					{
						if(plan.getTeatro().getFranquicia().equals(franquicia))
						{
							lista.agregarElementoFinal(plan);
							teatroActual = plan.getTeatro();
							horaActual = plan.getHoraFin().getTime();
							break;
						}
					}
					if (plan.getTeatro().getNombre().equals(teatroActual))
					{
						if(plan.getTeatro().getFranquicia().equals(franquicia))
						{
							lista.agregarElementoFinal(plan);
							teatroActual = plan.getTeatro();
							horaActual = plan.getHoraFin().getTime();
							break;
						}
					}
					else if (plan.getHoraInicio().getTime() >= horaActual)
					{
						VOTeatro teatroSiguiente = plan.getTeatro();
						double tiempoRecorrido = grafo.darPesoArco(teatroActual, teatroSiguiente);

						if (plan.getHoraInicio().getTime() >= (horaActual+tiempoRecorrido))
						{
							if(plan.getTeatro().getFranquicia().equals(franquicia))
							{
								lista.agregarElementoFinal(plan);
								teatroActual = plan.getTeatro();
								horaActual = plan.getHoraFin().getTime();
								break;
							}
						}
					}
				}
			}
		}
		else if(franja.equals("ma�ana"))
		{
			for(int i = 0; i<tablaPeliculas.darTamanio(); i++)
			{
				VOPelicula mejorpelicula = tablaPeliculas.darValor(i); 
				ListaEnlazadaSimple <VOPeliculaPlan> funcionSiguiente = mejorpelicula.darPeliculasSiguienteFuncion(fecha, horaActual);
				for(VOPeliculaPlan plan : funcionSiguiente)
				{
					if (lista.darNumeroElementos() == 0)
					{
						if(plan.getTeatro().getFranquicia().equals(franquicia))
						{
							if(plan.getHoraInicio().getTime()>dia1&&plan.getHoraInicio().getTime()<dia2)
							{
								lista.agregarElementoFinal(plan);
								teatroActual = plan.getTeatro();
								horaActual = plan.getHoraFin().getTime();
								break;
							}
						}
					}
					if (plan.getTeatro().getNombre().equals(teatroActual))
					{
						if(plan.getTeatro().getFranquicia().equals(franquicia))
						{
							if(plan.getHoraInicio().getTime()>dia1&&plan.getHoraInicio().getTime()<dia2)
							{
								lista.agregarElementoFinal(plan);
								teatroActual = plan.getTeatro();
								horaActual = plan.getHoraFin().getTime();
								break;
							}
						}
					}
					else if (plan.getHoraInicio().getTime() >= horaActual)
					{
						VOTeatro teatroSiguiente = plan.getTeatro();
						double tiempoRecorrido = grafo.darPesoArco(teatroActual, teatroSiguiente);

						if (plan.getHoraInicio().getTime() >= (horaActual+tiempoRecorrido))
						{
							if(plan.getTeatro().getFranquicia().equals(franquicia))
							{
								if(plan.getHoraInicio().getTime()>dia1&&plan.getHoraInicio().getTime()<dia2)
								{
									lista.agregarElementoFinal(plan);
									teatroActual = plan.getTeatro();
									horaActual = plan.getHoraFin().getTime();
									break;
								}
							}
						}
					}
				}
			}
		}
		else if(franja.equals("tarde"))
		{
			for(int i = 0; i<tablaPeliculas.darTamanio(); i++)
			{
				VOPelicula mejorpelicula = tablaPeliculas.darValor(i); 
				ListaEnlazadaSimple <VOPeliculaPlan> funcionSiguiente = mejorpelicula.darPeliculasSiguienteFuncion(fecha, horaActual);
				for(VOPeliculaPlan plan : funcionSiguiente)
				{
					if (lista.darNumeroElementos() == 0)
					{
						if(plan.getTeatro().getFranquicia().equals(franquicia))
						{
							if(plan.getHoraInicio().getTime()>tarde1&&plan.getHoraInicio().getTime()<tarde2)
							{
								lista.agregarElementoFinal(plan);
								teatroActual = plan.getTeatro();
								horaActual = plan.getHoraFin().getTime();
								break;
							}
						}
					}
					if (plan.getTeatro().getNombre().equals(teatroActual))
					{
						if(plan.getTeatro().getFranquicia().equals(franquicia))
						{
							if(plan.getHoraInicio().getTime()>tarde1&&plan.getHoraInicio().getTime()<tarde2)
							{
								lista.agregarElementoFinal(plan);
								teatroActual = plan.getTeatro();
								horaActual = plan.getHoraFin().getTime();
								break;
							}
						}
					}
					else if (plan.getHoraInicio().getTime() >= horaActual)
					{
						VOTeatro teatroSiguiente = plan.getTeatro();
						double tiempoRecorrido = grafo.darPesoArco(teatroActual, teatroSiguiente);

						if (plan.getHoraInicio().getTime() >= (horaActual+tiempoRecorrido))
						{
							if(plan.getTeatro().getFranquicia().equals(franquicia))
							{
								if(plan.getHoraInicio().getTime()>tarde1&&plan.getHoraInicio().getTime()<tarde2)
								{
									lista.agregarElementoFinal(plan);
									teatroActual = plan.getTeatro();
									horaActual = plan.getHoraFin().getTime();
									break;
								}
							}
						}
					}
				}
			}
		}
		for (VOPeliculaPlan peli : lista)
		{
			System.out.println(peli.getPelicula().getTitulo());
		}
		return lista;
	}

	@Override
	public ILista<VOPeliculaPlan> PlanPorGeneroYDesplazamiento(VOGeneroPelicula genero, int fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOPeliculaPlan> PlanPorGeneroDesplazamientoYFranquicia(VOGeneroPelicula genero, int fecha,
			VOFranquicia franquicia) {
		return null;
	}

	@Override
	public ILista<IEdge<VOTeatro>> generarMapa() {
		return null;
	}

	@Override
	public ILista<ILista<VOTeatro>> rutasPosible(VOTeatro origen, int n) {
		return null;
	}

	public EncadenamientoSeparadoTH <Integer, VOUsuario> darUsuarios()
	{
		return null;
	}


}
