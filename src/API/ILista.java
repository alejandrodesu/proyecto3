package API;


public interface ILista<T> extends Iterable <T>{
	public boolean isEmpty();

	public void agregarElementoFinal(T elem);
	
	public void agregarElementoInicio(T elem);
	
	public int darNumeroElementos();
	
	public T verPrimero() ;
	
	public ILista<T> darPrimerosElementos (int n);
}
