package data_structures;

import java.util.Iterator;

import API.ILista;


public class Queue <T> implements Iterable <T>{

	private ListaEnlazadaSimple <T> lista;

	public Iterator<T> iterator() {
		return lista.iterator();
	}

	public Queue()
	{
		lista = new ListaEnlazadaSimple<T>();
	}

	public void enqueue(T item)
	{
		lista.agregarElementoFinal(item);
	}

	public T dequeue ()
	{
		return lista.retornarPrimero();
	}

	public int size() 
	{		
		return lista.darNumeroElementos();
	}

	public boolean isEmpty() 
	{		
		return lista.isEmpty();
	}

	public ILista <T> darLista()
	{
		return lista;
	}

}
