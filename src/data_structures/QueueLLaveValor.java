package data_structures;

import java.util.Iterator;

public class QueueLLaveValor <K extends Comparable <K>, T> {
	private ListaEnlazada <K, T> lista;

	public Iterator<T> iterator() {
		return lista.iterator();
	}

	public QueueLLaveValor(){
		lista = new ListaEnlazada <K, T>();
	}

	public void enqueue(K llave, T item){
		lista.agregarElementoFinal(llave, item);
	}

	public T dequeue (){
		return lista.retornarPrimero();
	}

	public K verKey (){
		if (!isEmpty())
			return lista.darPrimerNodo().key;
		return null;
	}

	public int size() {		
		return lista.darNumeroElementos();
	}

	public boolean isEmpty() {		
		return lista.isEmpty();
	}

	public ListaEnlazada <K, T> darLista(){
		return lista;
	}
}
