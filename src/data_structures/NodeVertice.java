package data_structures;


public class NodeVertice <K extends Comparable <K>,V>
{
	public K key;
	public V item;
	public RedBlackBST<K, Edge<K>> edges;
	public boolean marcado;

	public NodeVertice (K key, V item){
		edges = new RedBlackBST<K, Edge<K>> ();
		this.item = item;
		this.key = key;
		this.marcado = false;
	}

	public int agregaEdge (Edge<K> edge, K key){

		int n = edges.size();
		edges.put(key, edge);
		return edges.size() - n;
	}

	public ListaEnlazada <K, Edge<K>> adj (){
		return edges.inorden(true);
	}

	public String toString (){
		return "K: " + key.toString() + "\tV: " + item.toString() + "\tGrado: " + edges.size();
	}
	public int grado(){
		return edges.size();
	}
}
