package data_structures;

public class IndexMinHeap <K extends Comparable <K>, T extends Comparable <T>> {

	private int N;
	private EncadenamientoSeparadoTH <K, Integer> tablaIndices;
	private T[] heap; 
	private K[] keys;

	public IndexMinHeap (int max){
		heap = (T[]) new Comparable [max+1];
		keys = (K[]) new Comparable [max+1]; 
		tablaIndices = new EncadenamientoSeparadoTH <K, Integer> (max*2);
	}

	public int darNumElementos (){
		return N;
	}

	public void agregar (K key, T elemento, boolean soloMin)
	{
		if (N < tamanoMax()){
			Integer indice = tablaIndices.darValor(key);
			if (indice != null){
				if (soloMin && !masPeque(elemento, heap[indice]))
					return;
				heap[indice] = elemento;
				swim(indice);
				sink(indice);
			}
			else{
				heap [++N] = elemento;
				keys [N] = key;
				tablaIndices.insertar(key, N);
				swim (N);
			}
		}
		else{
			resize(N*2);
			agregar(key, elemento, soloMin);
		}
	}

	private void resize(int size){
		T[] arreglo = (T[]) new Comparable[size+1];
		K[] arreglo2 = (K[]) new Comparable[size+1];
		for (int i = 1; i <= N; i++){
			arreglo [i] = heap[i];
			arreglo2 [i] = keys[i];
		}
		heap = arreglo;
		keys = arreglo2;
	}

	private void swim(int i) 
	{
		while (i != 1){
			int j = i/2;
			if (masPeque (heap[i], heap[j])) cambiar (i, j);
			else break;
			i = j;
		}
	}

	private void sink(int i) 
	{
		while (i*2 <= N){
			int j = i*2;
			if (j < N && masPeque(heap[j+1], heap[j])) j++;
			if (masPeque (heap[j], heap[i])) cambiar (i, j);
			else break;
			i = j;
		}
	}

	public T min (){
		if (!esVacia()){
			T min = heap[1];
			tablaIndices.insertar(keys[1], null);
			heap [1] = heap[N--];
			keys [1] = keys[N+1];
			heap [N+1] = null;
			keys [N+1] = null;
			if (!esVacia())
				tablaIndices.insertar(keys[1], 1);

			sink(1);
			return min;
		}
		return null;
	}

	public K minKeyVer (){
		return keys[1];
	}

	public T minVer (){
		return heap[1];
	}

	public T darValor (K key){
		Integer indice = tablaIndices.darValor(key);
		if (indice != null)
			return heap[indice];
		return null;
	}

	private boolean masPeque (T item1, T item2){
		return item1.compareTo(item2) < 0; 
	}

	private void cambiar (int i, int j){
		tablaIndices.insertar(keys[i], j);
		tablaIndices.insertar(keys[j], i);
		K tempK = keys [i];
		T temp = heap [i];
		heap [i] = heap [j];
		keys [i] = keys [j];
		heap [j] = temp;
		keys [j] = tempK;
	}

	public boolean esVacia (){
		return N == 0;
	}

	public boolean contiene (K key){
		return tablaIndices.darValor(key) != null;
	}

	public int tamanoMax(){
		return heap.length-1;
	}

	public ListaEnlazadaSimple <T> retornarOrdenado (boolean ascendente)
	{
		T[] arreglo = heap.clone();
		int n = N;

		ListaEnlazadaSimple <T> aux = new ListaEnlazadaSimple <T>();
		if (ascendente)
			for (int i = 0; i < n; i ++)
				aux.agregarElementoFinal(min());
		else
			for (int i = 0; i < n; i ++)
				aux.agregarElementoInicio(min());

		heap = arreglo;
		N = n;
		return  aux;
	}

	public ListaEnlazadaSimple<T> retornarNPrimeras (int n){
		if (n > N)
			n = N;
		T[] arreglo = heap.clone();
		int tamanio = N;

		ListaEnlazadaSimple <T> aux = new ListaEnlazadaSimple <T>();
		for (int i = 0; i < n; i ++)
			aux.agregarElementoFinal(min());

		heap = arreglo;
		N = tamanio;

		return aux;
	}
}
