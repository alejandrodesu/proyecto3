package data_structures;

public class ListaLlaveValorSecuencial <K extends Comparable <K>,V> {

	ListaEnlazada <K,V> lista;

	public ListaLlaveValorSecuencial() {
		lista = new ListaEnlazada <K,V> ();
	}
	public int darTamanio(){
		return lista.darNumeroElementos();
	}
	public boolean estaVacia(){
		return lista.isEmpty();
	}
	public boolean tieneLlave (K llave){
		return lista.tieneLlave(llave);
	}
	public V darValor(K llave){
		return lista.darValor(llave);
	}
	public int insertar(K llave, V valor){
		return lista.insertar(llave, valor);
	}
	public Iterable<V> values(){
		return lista;
	}
	public ListaEnlazada <K,V> darLista(){
		return lista;
	}

}
