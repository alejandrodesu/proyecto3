package data_structures;






public interface ILista2<T> extends Iterable <T>{
	
	public boolean isEmpty();

	public void agregarElementoFinal(T elem);
	
	public void agregarElementoInicio(T elem);
	
	public int darNumeroElementos();
	
	public T verPrimero() ;
}
