package data_structures;

import java.util.Iterator;

import API.ILista;


public class ListaEnlazadaSimple <T> implements Iterable <T>, ILista <T>

{
	private NodoSimple <T> primero;
	private NodoSimple <T> ultimo;
	int N;

	private class Iterador implements Iterator<T>
	{
		private NodoSimple <T> actualI =  primero;

		public boolean hasNext(){return actualI != null;}

		public T next()
		{
			T item = actualI.item;
			actualI = actualI.sig;
			return item; 
		}

		@Override
		public void remove() {
			// TODO Auto-generated method stub

		}
	}

	public Iterator <T> iterator() {
		return new Iterador();
	}

	public void agregarElementoFinal(T elem) {
		NodoSimple <T> ultimoAntiguo = ultimo;
		ultimo = new NodoSimple <T>();
		ultimo.item = elem;
		ultimo.sig = null;
		if (isEmpty())
			primero = ultimo;
		else
			ultimoAntiguo.sig = ultimo;
		N++;
	}

	public void agregarElementoInicio(T elem) {

		NodoSimple <T> primeroAntiguo = primero;
		primero = new NodoSimple <T>();
		primero.item=elem;
		primero.sig = primeroAntiguo;
		if(isEmpty())
			ultimo = primero;
		N++;
	}

	public boolean isEmpty() {
		return N==0;
	}

	public T verPrimero() {
		if(!isEmpty())
			return primero.item;
		else
			return null;
	}

	public T verUltimo() {
		if(!isEmpty())
			return ultimo.item;
		else
			return null;
	}

	public T retornarPrimero() {
		T item = null;
		if(!isEmpty())
		{
			item = primero.item;
			primero = primero.sig;
			N--;
			if(isEmpty())
				ultimo=null;
		}
		return item;
	}

	public T retornarUltimo() {
		T item = null;
		if(!isEmpty())
		{
			item = ultimo.item;
			ultimo = primero;
			for (int i = 0; i< N-1; i++)
				ultimo= ultimo.sig;
			N--;
		}
		return item;
	}

	public int darNumeroElementos() {
		return N;
	}

	public T darElemento(int pos) {
		if (!isEmpty() && pos > 0 && pos <=N)
		{
			NodoSimple <T> actual = primero;
			int posicion = 1;
			while (pos != posicion)
			{
				actual = actual.sig;
				posicion++;
			}
			return actual.item;
		}
		else
			return null;
	}

	public T eliminarElemento(int pos) {
		if (isEmpty())
			return null;
		else if(pos == 1)
		{
			T item = primero.item;
			N--;
			if(isEmpty())
			{
				primero = null;
				ultimo = null;
			}
			else
				primero = primero.sig;
			return item;
		}
		else if(pos > 0 && pos <= N)
		{
			int posicion = 1;
			NodoSimple <T> anterior = primero;

			for (int i = posicion; i<pos; i++)
				anterior = anterior.sig;
			NodoSimple <T> aEliminar = anterior.sig;
			T item = aEliminar.item;
			if(aEliminar.sig == null)
			{
				anterior.sig = null;
				ultimo = anterior;
			}
			else
				anterior.sig = aEliminar.sig;
			N--;
			return item;
		}
		else
			return null;		
	}

	public ListaEnlazadaSimple <T> darPrimerosElementos (int n){
		ListaEnlazadaSimple <T> lista = new ListaEnlazadaSimple <T>(); 
		int j = 0;
		if (n > N) n = N;
		for(T item: this){
			if (j != n){
				lista.agregarElementoFinal(item);
				j++;
			}
			else break;
		}
		return lista;
	}

	public NodoSimple <T> darPrimerNodo (){
		return primero;
	}

	public NodoSimple <T> darUltimoNodo (){
		return ultimo;
	}

	public void setPrimerNodo (NodoSimple <T> prim)
	{
		primero = prim;
		ultimo = prim;
		if (ultimo != null){
			N = 1;
			while ( ultimo.sig != null){
				ultimo = ultimo.sig;
				N++;
			}
		}
		else
			N = 0;
	}

	public ListaEnlazadaSimple <T> unirLista (ListaEnlazadaSimple <T> listaAUnir){
		if (listaAUnir.isEmpty())
			return this;
		else if (isEmpty())
			primero = listaAUnir.darPrimerNodo();
		else
			ultimo.sig = listaAUnir.darPrimerNodo();

		N += listaAUnir.darNumeroElementos();
		ultimo = listaAUnir.darUltimoNodo();
		return this;
	}

	public String toString(){
		String prim = "null";
		String ult = "null";
		if (primero != null){
			prim = primero.toString();
			ult = ultimo.toString();
		}
		return "N:"+N +"\t First:"+ prim + "\tLast:" + ult;
	}
}
