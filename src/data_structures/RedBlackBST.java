package data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class RedBlackBST <K extends Comparable <K>, V> implements Iterable <V> {

	private Node <K, V> root;
	private static final boolean RED = true;
	private static final boolean BLACK = false;
	private int N;

	private class Node <K extends Comparable <K>, V> {
		Node <K, V> left, right;
		boolean color;
		K key;
		V value;

		Node (K k, V v, boolean c){
			this.key = k;
			this.value = v;
			this.color = c;
		}
	}

	public Iterator <V> values(){
		return inorden(true).iterator();
	}

	public Iterator<V> iterator() {
		return inorden(true).iterator();
	}

	public boolean isEmpty (){
		return N == 0;
	}
	public int size(){
		return N;
	}

	/**
	private int size(Node <K, V> nodo) {
		if (nodo == null)
			return 0;
		return 1 + size(nodo.left) + size(nodo.right);
	}*/

	public int height (){
		return height(root);
	}
	private int height (Node <K, V> nodo){
		if (nodo == null)
			return 0;
		else{
			int altIzq = height(nodo.left);
			int altDer = height(nodo.right);
			return (altIzq < altDer)? altDer+1: altIzq+1 ;
		}
	}

	private boolean isRed(Node <K, V> nodo){
		if (nodo == null) return BLACK;
		return nodo.color == RED;
	}
	private void flipColors(Node <K, V> nodo){
		nodo.color = RED;
		nodo.left.color = BLACK;
		nodo.right.color = BLACK;
	}

	private Node <K, V> rotateLeft (Node <K, V> h)
	{
		Node <K, V> x = h.right;
		h.right = x.left;
		x.left = h;
		x.color = h.color;
		h.color = RED;
		return x;
	}
	private Node <K, V> rotateRight(Node <K, V> h)
	{
		Node <K, V> x = h.left;
		h.left = x.right;
		x.right = h;
		x.color = h.color;
		h.color = RED;
		return x;
	}

	public void put (K key, V value)
	{
		root = put(root, key, value);
		root.color = BLACK;
	}
	private Node <K, V> put (Node <K, V> nodo, K key, V value)
	{
		if (nodo == null) {
			N++;
			return new Node <K, V> (key, value, RED);
		}

		int compare = key.compareTo(nodo.key);
		if (compare < 0) nodo.left = put(nodo.left, key, value) ;
		else if (compare > 0) nodo.right = put(nodo.right, key, value);
		else nodo.value = value;

		if (!isRed(nodo.left) && isRed(nodo.right)) nodo = rotateLeft(nodo);
		if (isRed(nodo.left) && isRed(nodo.left.left)) nodo = rotateRight(nodo);
		if (isRed(nodo.left) && isRed(nodo.right)) flipColors(nodo);

		return nodo;
	}

	public V get (K key ) throws NoSuchElementException{
		return get (root, key);
	}
	public boolean contains (K key){
		try{
			get (root, key);
			return true;
		}
		catch (NoSuchElementException e){
			return false;
		}
	}
	private V get(Node <K, V> nodo, K key) throws NoSuchElementException {
		if (nodo == null) throw new NoSuchElementException("");
		int compare = key.compareTo(nodo.key);
		if (compare < 0) return get(nodo.left, key);
		else if (compare > 0) return get(nodo.right, key);
		else return nodo.value;
	}

	public V minValue (){
		return minNode().value;
	}
	public K min (){
		return minNode().key;
	}
	private Node <K, V> minNode (){
		Node <K, V> actual = root; 
		while (actual.left != null)
			actual = actual.left;
		return actual;
	}

	public V maxValue (){
		return maxNode().value;
	}
	public K max (){
		return maxNode().key;
	}
	private Node <K, V> maxNode (){
		Node <K, V> actual = root; 
		while (actual.right != null)
			actual = actual.right;
		return actual;
	}

	public ListaEnlazada <K, V> darLlavesValoresEnRango (K min, K max)
	{
		ListaEnlazada<K, V> lista = new ListaEnlazada<K,V>();
		aniadirValoresRango(root, lista, min, max);
		return lista;
	}
	private void aniadirValoresRango (Node <K, V> nodo, ListaEnlazada<K, V> lista, K min, K max)
	{
		if (nodo == null) return;
		int comparateLow = min.compareTo(nodo.key);
		int comparateHigh = max.compareTo(nodo.key);
		if (comparateLow < 0) aniadirValoresRango(nodo.left, lista, min, max);
		if (comparateLow <= 0 && comparateHigh >= 0) lista.agregarElementoFinal(nodo.key, nodo.value);
		if (comparateHigh > 0) aniadirValoresRango(nodo.right, lista, min, max);
	}

	public ListaEnlazada<K, V> inorden (boolean ascendente)
	{
		ListaEnlazada<K, V> lista = new ListaEnlazada<K,V>();
		inorden(root, lista, ascendente);
		return lista;
	}
	private void inorden(Node<K, V> nodo, ListaEnlazada<K, V> lista, boolean ascendente) {
		if (nodo == null) return;

		inorden(nodo.left, lista, ascendente);
		if (ascendente)
			lista.agregarElementoFinal(nodo.key, nodo.value);
		else
			lista.agregarElementoInicio(nodo.key, nodo.value);
		inorden(nodo.right, lista, ascendente);
	}

	public boolean check (){
		return check(root);
	}

	private boolean check(Node<K, V> nodo) {
		if (nodo == null)
			return true;
		if (isRed(nodo.right))
			return false;
		K llave = nodo.key;
		if (nodo.left != null)
			if (llave.compareTo(nodo.left.key) < 0)
				return false;
		if (nodo.right != null)
			if (llave.compareTo(nodo.right.key) > 0)
				return false;
		if (alturaNegra(nodo.left) != alturaNegra(nodo.right))
			return false;
		if (!check(nodo.left) || !check(nodo.right))
			return false;
		return true;
	}

	public int alturaNegra (){
		return alturaNegra (root);
	}

	private int alturaNegra (Node<K, V> nodo) {
		if (nodo == null)
			return 0;
		else{
			int contar = (isRed(nodo))? 0:1;
			int altIzq = alturaNegra(nodo.left);
			int altDer = alturaNegra(nodo.right);
			if (altIzq != altDer)
				System.out.println("No est� balanceado-negro");
			return (altIzq < altDer)? altDer+contar: altIzq+contar ;
		}
	}
}
