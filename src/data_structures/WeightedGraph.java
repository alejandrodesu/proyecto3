package data_structures;

import java.util.*;

import API.ILista;
import data_structures.NodeVertice;
public class WeightedGraph <K extends Comparable<K> , V> implements Iterable <K>{

	private int E;                 
	private EncadenamientoSeparadoTH <K , NodeVertice <K, V>> vertices;    
	private Integer componentesConectados;

	public WeightedGraph(int vertices){
		this.E = 0;
		this.vertices = new EncadenamientoSeparadoTH <K, NodeVertice <K, V>> (vertices);
	}

	public int numVertices(){
		return vertices.darTamanio();
	}

	public Iterable <Edge<K>> adj(K  id)throws NoSuchElementException{
		NodeVertice <K, V> vertice = vertices.darValor(id);
		if (vertice == null)
			throw new NoSuchElementException();
		return vertice.adj();
	}

	public V darVertice(K id) throws NoSuchElementException{
		NodeVertice <K, V> vertice = vertices.darValor(id);
		if (vertice == null)
			throw new NoSuchElementException();
		return vertice.item;
	}

	public void agregarVertice(K id, V
			infoVer){
		componentesConectados = null;
		NodeVertice <K, V> node = new NodeVertice<K, V>(id, infoVer); 
		vertices.insertar(id, node);
	}

	public int numArcos(){
		return E;
	}

	public double darPesoArco(K idOrigen, K idDestino) throws NoSuchElementException {
		NodeVertice <K, V> node = vertices.darValor(idOrigen);
		if (node != null)
			for (Edge<K> edge: node.adj()){
				if (edge.other(idOrigen).equals(idDestino))
					return edge.weight();
			}
		throw new NoSuchElementException();
	}
//	public darPesoCamino()
//	{
//		
//	}


	public void agregarArco (K idOrigen, K
			idDestino, double peso)  throws NoSuchElementException {
		componentesConectados = null;
		NodeVertice <K, V> node = vertices.darValor(idOrigen);
		NodeVertice <K, V> node2 = vertices.darValor(idDestino);
		if (node != null && node2 != null){
			Edge<K> edge = new Edge<K>(idOrigen, idDestino, peso);
			E += node.agregaEdge(edge, idDestino);
			node2.agregaEdge(edge, idOrigen);
		}
		else{
			System.out.println(node.toString()+"//"+ idDestino.toString());
			throw new NoSuchElementException();

		}
	}
	public Iterator<K> darVertices(){
		return vertices.keys();
	}

	public int darGrado(K id) throws NoSuchElementException{
		NodeVertice <K, V> node = vertices.darValor(id);
		if (node != null)
			return node.grado();
		throw new NoSuchElementException();
	}

	public Iterator<K> darVerticesAdyacentes(K id) throws NoSuchElementException{
		NodeVertice <K, V> node = vertices.darValor(id);
		if (node != null)
			return node.adj().iteratorKeys();
		throw new NoSuchElementException();
	}

	public void desmarcar(){
		for (NodeVertice <K, V> nodo : vertices)
			nodo.marcado = false;
	}
	public boolean contiene (K id){
		return vertices.darValor(id) != null;
	}

	public EncadenamientoSeparadoTH <K,NodoCamino<K>> DFS(K idOrigen) throws NoSuchElementException{
		NodeVertice <K, V> node = vertices.darValor(idOrigen);
		if (node != null){
			node.marcado = true;
			EncadenamientoSeparadoTH <K,NodoCamino<K>> tabla = new EncadenamientoSeparadoTH <K,NodoCamino<K>>(numVertices());
			tabla.insertar(idOrigen, new NodoCamino<K>(idOrigen, idOrigen, 0, 0));
			for (Edge <K> edge : node.adj()){
				int longitud = 0;
				double peso = 0.0;
				NodeVertice <K, V> nodeInterno = vertices.darValor(edge.other(idOrigen));
				if (!nodeInterno.marcado)
					dfs (nodeInterno, edge, tabla, peso, longitud);
			}
			desmarcar();
			return tabla;
		}
		else
			throw new NoSuchElementException();
	}

	private void dfs (NodeVertice <K, V> node, Edge <K> edge, EncadenamientoSeparadoTH <K,NodoCamino<K>> tabla, double peso, int longitud){
		NodoCamino<K> nodoCamino = edge.camino(++longitud, peso);
		tabla.insertar(node.key, nodoCamino);
		peso = nodoCamino.weight;
		node.marcado = true;

		for (Edge <K> edgeInterno : node.adj()){
			NodeVertice <K, V> nodeInterno = vertices.darValor(edgeInterno.other(node.key));
			if (!nodeInterno.marcado)
				dfs (nodeInterno, edgeInterno, tabla, peso, longitud);
		}
	}


	public EncadenamientoSeparadoTH <K,NodoCamino<K>> BFS(K idOrigen){
		NodeVertice <K, V> node = vertices.darValor(idOrigen);
		if (node != null){
			EncadenamientoSeparadoTH <K,NodoCamino<K>> tabla = new EncadenamientoSeparadoTH <K,NodoCamino<K>>(numVertices());
			bfs (node, tabla);
			desmarcar();
			return tabla;
		}
		else
			throw new NoSuchElementException();
	}

	private void bfs (NodeVertice <K, V> node,  EncadenamientoSeparadoTH <K,NodoCamino<K>> tabla){
		QueueLLaveValor <Double, NodeVertice <K, V>> cola2 = new QueueLLaveValor <Double, NodeVertice <K, V>>();
		tabla.insertar(node.key, new NodoCamino<K>(node.key, node.key, 0.0, 0));
		cola2.enqueue(0.0, node);
		node.marcado = true;
		int longitud = 1;

		while (!cola2.isEmpty()){
			int tamanioCola = cola2.size();
			for (int i = 0; i < tamanioCola; i++){
				double peso = cola2.verKey();
				NodeVertice <K, V> nodoInterno = cola2.dequeue();

				for (Edge <K> edgeInterno : nodoInterno.adj()){
					NodeVertice <K, V> nodoInterno2 = vertices.darValor(edgeInterno.other(nodoInterno.key));
					if (!nodoInterno2.marcado){
						NodoCamino<K> nodoCamino = edgeInterno.camino(longitud, peso);
						tabla.insertar(nodoInterno2.key, nodoCamino);
						nodoInterno2.marcado = true;
						cola2.enqueue(nodoCamino.weight, nodoInterno2);
					}
				}
			}
			longitud++;
		}
	}

	public Iterable <NodoCamino<K>> darCaminoDFS(K
			idOrigen, K idDestino){
		return darCamino(idOrigen, idDestino, DFS(idOrigen));
	}

	public Iterable <NodoCamino<K>> darCaminoBFS (K
			idOrigen, K idDestino){
		return darCamino(idOrigen, idDestino, BFS(idOrigen));
	}

	public Iterable <NodoCamino<K>> darCaminoDijkstra (K
			idOrigen, K idDestino){
		return darCamino(idOrigen, idDestino, dijkstra(idOrigen));
	}

	private Iterable <NodoCamino<K>> darCamino(K
			idOrigen, K idDestino, EncadenamientoSeparadoTH <K,NodoCamino<K>> camino){
		Stack <NodoCamino<K>> stack = new Stack<>();
		if (camino.tieneLlave(idDestino)){
			while (idDestino != idOrigen){
				NodoCamino<K> nodoCamino = camino.darValor(idDestino);
				stack.push(nodoCamino);
				idDestino = nodoCamino.other(idDestino);
			}
		}else
			return null;
		return stack;
	}

	public EncadenamientoSeparadoTH<K , NodoCamino<K>> dijkstra (K idOrigen){
		EncadenamientoSeparadoTH<K , NodoCamino<K>> tabla = new EncadenamientoSeparadoTH<> (numVertices());
		IndexMinHeap <K, NodoCamino<K>> pq = new IndexMinHeap <>(numVertices());

		pq.agregar(idOrigen, new NodoCamino<K>(idOrigen, idOrigen, 0, 0), true);
		while (!pq.esVacia()){
			K v = pq.minKeyVer();
			NodoCamino<K> nodoCamino = pq.min();
			tabla.insertar(v, nodoCamino);
			for (Edge<K> edge : adj(v)){
				K w = edge.other(v);
				if (!tabla.tieneLlave(w)){
					NodoCamino<K> nodoCaminoInterno = edge.camino(nodoCamino.lenght+1, nodoCamino.weight);
					pq.agregar(w, nodoCaminoInterno, true);
				}
			}
		}
		return tabla;
	}
	public EncadenamientoSeparadoTH<K , Edge<K>> prim (){
		if (componentesConectados() != 1)
			return null;
		K idOrigen = vertices.keys().next();
		if (idOrigen != null)
			return prim(idOrigen);
		return null;
	}

	public EncadenamientoSeparadoTH<K , Edge<K>> prim (K idOrigen){
		EncadenamientoSeparadoTH<K , Edge<K>> tabla = new EncadenamientoSeparadoTH<> (numVertices());
		IndexMinHeap <K, Edge<K>> pq = new IndexMinHeap <>(numVertices());
		pq.agregar(idOrigen, new Edge<K>(idOrigen, idOrigen, 0.0), true);
		while (!pq.esVacia()){
			K v = pq.minKeyVer();
			Edge<K> minEdge = pq.min();
			tabla.insertar(v, minEdge);
			for (Edge<K> edge : adj(v)){
				K w = edge.other(v);
				if (!tabla.tieneLlave(w)){
					pq.agregar(w, edge, true);
				}
			}
		}
		tabla.insertar(idOrigen, null);
		return tabla;
	}

	public int componentesConectados (){
		if (componentesConectados != null)
			return componentesConectados;
		int componentes = 0;
		for (NodeVertice <K, V> nodo: vertices){
			if (!nodo.marcado){
				dfsContarComponentes(nodo);
				componentes++;
			}
		}
		desmarcar();
		componentesConectados = componentes;
		return componentes;
	}

	private void dfsContarComponentes (NodeVertice <K, V> nodo){
		nodo.marcado = true;
		for (Edge <K> edgeInterno : nodo.adj()){
			NodeVertice <K, V> nodeInterno = vertices.darValor(edgeInterno.other(nodo.key));
			if (!nodeInterno.marcado)
				dfsContarComponentes (nodeInterno);
		}
	}

	@Override
	public Iterator<K> iterator() {
		return darVertices();
	}
}


