package data_structures;

public class NodoCamino <K> implements Comparable <NodoCamino<K>>{

	private K idFinal;
	private K idAdy;
	public double weight;
	public int lenght;

	public NodoCamino (K idFinal, K idAdy, double weight, int length){
		this.idFinal = idFinal;
		this.idAdy = idAdy;
		this.weight = weight;
		this.lenght = length;
	}

	public K either() {
		return idFinal;
	}

	public K other(K vertice) {
		return (vertice.equals(idFinal))? idAdy:idFinal;
	}
	
	public String toString (){
		return  idAdy + "<->" + idFinal + " - W: " + weight + " - L: " + lenght; 
	}

	@Override
	public int compareTo(NodoCamino<K> arg) {
		if (weight == arg.weight)
			return 0;
		return (weight <= arg.weight)? -1:1;
	}
}
