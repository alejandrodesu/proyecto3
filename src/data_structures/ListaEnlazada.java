package data_structures;

import java.util.Iterator;

public class ListaEnlazada <K extends Comparable <K>,V> implements Iterable <V>
{
	private Nodo <K,V> primero;
	private Nodo <K,V> ultimo;
	int N;

	private class Iterador implements Iterator <K>
	{
		private Nodo <K,V> actualI =  primero;

		public boolean hasNext() {return actualI != null;}

		public K next()
		{
			K key = actualI.key;
			actualI = actualI.sig;
			return key; 
		}

		@Override
		public void remove() {}
	}

	private class IteratorValues implements Iterator <V>
	{
		private Nodo <K,V> actualI =  primero;

		public boolean hasNext() {return actualI != null;}

		public V next()
		{
			V value = actualI.item;
			actualI = actualI.sig;
			return value; 
		}

		@Override
		public void remove() {}
	}

	public Iterator <V> iterator() {
		return new IteratorValues();
	}

	public Iterator <K> iteratorKeys (){
		return new Iterador();
	}

	public void agregarElementoFinal(K llave, V elem) {
		Nodo <K,V> ultimoAntiguo = ultimo;
		ultimo = new Nodo <K,V>();
		ultimo.item = elem;
		ultimo.key = llave;
		ultimo.sig = null;
		if (isEmpty())
			primero = ultimo;
		else
			ultimoAntiguo.sig = ultimo;
		N++;
	}

	public void agregarElementoInicio(K llave, V elem) {

		Nodo <K,V> primeroAntiguo = primero;
		primero = new Nodo <K,V>();
		primero.item = elem;
		primero.key = llave;
		primero.sig = primeroAntiguo;
		if(isEmpty())
			ultimo = primero;
		N++;
	}

	public boolean isEmpty() {
		return N==0;
	}

	public V verPrimero() {
		if(!isEmpty())
			return primero.item;
		else
			return null;
	}

	public V verUltimo() {
		if(!isEmpty())
			return ultimo.item;
		else
			return null;
	}

	public V retornarPrimero() {
		V item = null;
		if(!isEmpty())
		{
			item = primero.item;
			primero = primero.sig;
			N--;
			if(isEmpty())
				ultimo=null;
		}
		return item;
	}

	public V retornarUltimo() {
		V item = null;
		if(!isEmpty())
		{
			item = ultimo.item;
			ultimo = primero;
			for (int i = 0; i< N-1; i++)
				ultimo= ultimo.sig;
			N--;
		}
		return item;
	}

	public int darNumeroElementos() {
		return N;
	}

	public boolean tieneLlave (K llave)
	{
		Iterator <K> iter = iteratorKeys();
		while (iter.hasNext())
			if (iter.next().equals(llave))
				return true;
		return false;
	}

	public V darValor (K llave)
	{
		Nodo <K,V> actual = primero;
		while (actual != null)
		{
			if (actual.key.equals(llave))
				return actual.item;
			actual = actual.sig;
		}		
		return null;
	}

	public int insertar (K llave, V elem)
	{
		Nodo <K,V> actual = primero;
		if (actual != null)
		{
			if (actual.key.equals(llave)){
				if (elem != null){
					actual.item = elem;
					return 0;
				}
				else{
					eliminarElemento(1);
					return -1;
				}
			}
			else
				while (actual.sig != null)
				{
					if (actual.sig.key.equals(llave)){
						if (elem != null){
							actual.sig.item = elem;
							return 0;
						}
						else{
							actual.sig = actual.sig.sig;
							if (actual.sig == null)
								ultimo = actual;
							N--;
							return -1;
						}
					}
					actual = actual.sig;
				}	
		}
		if (elem != null){
			agregarElementoFinal(llave, elem);
			return 1;	
		}
		else return 0;
	}

	public V darElemento(int pos) {
		if (!isEmpty() && pos > 0 && pos <=N)
		{
			Nodo <K,V> actual = primero;
			int posicion = 1;
			while (pos != posicion)
			{
				actual = actual.sig;
				posicion++;
			}
			return actual.item;
		}
		else
			return null;
	}

	public V eliminarElemento(int pos) {
		if (isEmpty())
			return null;
		else if(pos == 1)
		{
			V item = primero.item;
			N--;
			if(isEmpty())
			{
				primero = null;
				ultimo = null;
			}
			else
				primero = primero.sig;
			return item;
		}
		else if(pos > 0 && pos <= N)
		{
			int posicion = 1;
			Nodo <K,V> anterior = primero;

			for (int i = posicion; i<pos; i++)
				anterior = anterior.sig;
			Nodo <K,V> aEliminar = anterior.sig;
			V item = aEliminar.item;
			if(aEliminar.sig == null)
			{
				anterior.sig = null;
				ultimo = anterior;
			}
			else
				anterior.sig = aEliminar.sig;
			N--;
			return item;
		}
		else
			return null;		
	}

	public ListaEnlazada <K,V> darPrimerosElementos (int n){
		ListaEnlazada <K,V> lista = new ListaEnlazada <K,V>(); 
		int j = 0;
		if (n > N) n = N;
		Nodo <K, V> actual = primero;
		while(actual != null ){
			if (j != n)
			{
				lista.agregarElementoFinal(actual.key, actual.item);
				actual = actual.sig;
				j++;
			}
			else break;
		}
		return lista;
	}

	public Nodo <K,V> darPrimerNodo (){
		return primero;
	}

	public Nodo <K,V> darUltimoNodo (){
		return ultimo;
	}

	public void setPrimerNodo (Nodo <K,V> prim)
	{
		primero = prim;
		ultimo = prim;
		if (ultimo != null){
			N = 1;
			while ( ultimo.sig != null){
				ultimo = ultimo.sig;
				N++;
			}
		}
		else
			N = 0;
	}

	public ListaEnlazada <K,V> unirLista (ListaEnlazada <K,V> listaAUnir)
	{
		if (listaAUnir.isEmpty())
			return this;
		else if (isEmpty())
			primero = listaAUnir.darPrimerNodo();
		else
			ultimo.sig = listaAUnir.darPrimerNodo();

		N += listaAUnir.darNumeroElementos();
		ultimo = listaAUnir.darUltimoNodo();
		return this;
	}

	//	public void agregarElementoOrdenado(V item)
	//	{
	//		Nodo <K,V> actual = primero;
	//		if (actual == null || masGrande(item, actual.item))
	//		{
	//			agregarElementoInicio(item);
	//		}
	//		else
	//		{
	//			while (actual != null && actual.sig != null)
	//			{
	//				if (masGrande(item, actual.sig.item)) break;
	//				actual = actual.sig;
	//			}
	//			Nodo <K,V> agregar = new Nodo<K,V>();
	//			agregar.item = item;
	//			agregar.sig = actual.sig;
	//			
	//			actual.sig = agregar;
	//			N++;
	//		}
	//	}

	private boolean comparar (K item1, K item2, boolean ascendente)
	{
		if (ascendente)
			return item1.compareTo(item2) > 0;
			else
				return item1.compareTo(item2) < 0;
	}

	public void ordenar (boolean ascendente){
		for (int i = N; i >0; i--){
			Nodo <K,V> actual = primero; 
			for (int j = 1; j <i; j++){
				if (comparar (actual.key, actual.sig.key, ascendente))
					cambiar (actual, actual.sig);
				actual = actual.sig;
			}
		}
	}

	private void cambiar (Nodo <K, V> uno, Nodo <K , V> dos){
		K keyUno = uno.key;
		V valueUno = uno.item;

		uno.key = dos.key;
		uno.item = dos.item;

		dos.key = keyUno;
		dos.item = valueUno;
	}

	public String toString(){
		String prim = "null";
		String ult = "null";
		if (primero != null){
			prim = primero.toString();
			ult = ultimo.toString();
		}
		return "N:"+N +"\tFirst:"+ prim + "\tLast:" + ult;
	}
}
