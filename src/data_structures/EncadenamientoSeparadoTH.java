package data_structures;

import java.util.Iterator;
import java.util.Random;


public class EncadenamientoSeparadoTH <K extends Comparable <K>,V> implements Iterable <V>{

	private int min, N, M;
	private int prime = 109345121; //primo predeterminado;
	private long scale, shift;
	private ListaLlaveValorSecuencial <K,V> [] tabla;
	private ListaEnlazada <K, V> lista;


	public EncadenamientoSeparadoTH (int m){ 
		min = m;
		M = m;
		Random random = new Random();
		scale = random.nextInt(prime-1) + 1;
		shift = random.nextInt(prime);
		initTabla();
	}

	private void initTabla()
	{
		N = 0;
		tabla = new ListaLlaveValorSecuencial [M];
		for (int i = 0; i < M; i++)
			tabla[i] = new ListaLlaveValorSecuencial<K,V>();
	}

	public void crearIterador (){
		lista = new ListaEnlazada <K, V>();
		for (int i = 0; i < M; i++)
			lista = lista.unirLista(tabla[i].darLista());
	}
	public int darTamanio(){
		return N;
	}
	public int darCapacidad(){
		return M;
	}
	public boolean estaVacia(){
		return N == 0;
	}

	public boolean tieneLlave(K llave){
		return tabla[hash(llave)].tieneLlave(llave);
	}

	private int hash(K llave){
		return (int)((Math.abs(llave.hashCode())* scale + shift) % prime )% M;
	}

	public V darValor(K llave){
		return  tabla[hash(llave)].darValor(llave);
	}

	public void insertar(K llave, V valor){
		this.lista = null;
		N += tabla[hash(llave)].insertar(llave, valor);
		if (N/M >= 4)
			rehash(M*2);
		else if (M/2 >= min && N/M <= 1)
			rehash(M/2);
	}

	private void rehash(int size) {
		if (lista == null)
			crearIterador();

		int n = N;
		M = size;
		initTabla();

		Nodo <K,V> actual = lista.darPrimerNodo();
		while (actual != null){
			N += tabla[hash(actual.key)].insertar(actual.key, actual.item);
			actual = actual.sig;
		}
		assert (n == N);
	}

	public int[] darLongitudListas(){
		int[] longitudes = new int[M];
		for (int i = 0; i < M; i++)
			longitudes[i] = tabla[i].darTamanio();

		return longitudes;
	}

	public ListaEnlazada <K, V> darLista(){
		if (lista == null)
			crearIterador();
		return lista;
	}

	public Iterator<K> keys (){
		return darLista().iteratorKeys();
	}

	public Iterator<V> iterator() {
		return darLista().iterator();
	}

	public String toString (){
		return "M:" + M + " /N:" + N + " /FC:" + N/M + " /min:" + min;
	}
}
