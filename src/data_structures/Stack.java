/**
 * 
 */
package data_structures;

import java.util.Iterator;


public class Stack <T> implements Iterable <T> {

	private ListaEnlazadaSimple <T> lista;

	public Iterator<T> iterator() {
		return lista.iterator();
	}

	public Stack ()
	{
		lista = new ListaEnlazadaSimple <T>();
	}

	public void push (T item)
	{
		lista.agregarElementoInicio(item);
	}

	public T pop()
	{
		return lista.retornarPrimero();
	}

	public boolean isEmpty()
	{
		return lista.isEmpty();
	}

	public int size()
	{
		return lista.darNumeroElementos();
	} 
	
	public ListaEnlazadaSimple <T> darLista()
	{
		return lista;
	}
}
