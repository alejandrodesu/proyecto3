package data_structures;

public class FixedHeap <T extends Comparable <T>>{

	private T[] heap; 
	private int N;


	public FixedHeap (int max){
		heap = (T[]) new Comparable [max+1]; 
	}

	public int darNumElementos (){
		return N;
	}

	public void agregar (T elemento)
	{
		if (N < tamanoMax()){
			heap [++N] = elemento;
			swim (N);
		}
		else {
			if (masPeque(heap[1], elemento)){
				heap[1] = elemento;
				sink(1);
			}
		}
	}


	private void swim(int n) 
	{
		while (n != 1)
		{
			int j = n/2;
			if (masPeque (heap[n], heap[j])) cambiar (n, j);
			else break;
			n = j;
		}
	}

	public T max ()
	{
		if (!esVacia())
		{
			T max = heap[1];
			heap [1] = heap[N--];
			heap [N+1] = null;
			sink(1);
			return max;
		}
		return null;
	}

	private void sink(int i) 
	{
		while (i*2 <= N)
		{
			int j = i*2;
			if (j < N && masPeque(heap[j+1], heap[j])) j++;
			if (masPeque (heap[j], heap[i])) cambiar (i, j);
			else break;
			i = j;
		}
	}

	private boolean masPeque (T item1, T item2){
		return item1.compareTo(item2) < 0; 
	}

	private void cambiar (int i, int j){
		T  temp = heap [i];
		heap [i] = heap [j];
		heap [j] = temp;
	}

	public boolean esVacia (){
		return N == 0;
	}

	public int tamanoMax(){
		if (heap != null)
			return heap.length-1;
		return 0;
	}

	public ListaEnlazadaSimple <T> retornarOrdenado (boolean descendente)
	{
		T[] arreglo = heap.clone();
		int n = N;

		ListaEnlazadaSimple <T> aux = new ListaEnlazadaSimple <T>();
		if (descendente)
			for (int i = 0; i < n; i ++)
				aux.agregarElementoInicio(max());
		else
			for (int i = 0; i < n; i ++)
				aux.agregarElementoFinal(max());

		heap = arreglo;
		N = n;
		return  aux;
	}

	public ListaEnlazadaSimple<T> retornarNPrimeras (int n){
		if (n > N)
			n = N;
		T[] arreglo = heap.clone();
		int tamanio = N;

		ListaEnlazadaSimple <T> aux = new ListaEnlazadaSimple <T>();
		for (int i = 0; i < n; i ++)
			aux.agregarElementoFinal(max());

		heap = arreglo;
		N = tamanio;

		return aux;
	}
}
