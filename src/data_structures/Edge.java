package data_structures;

import API.IEdge;

public class Edge <K> implements Comparable <Edge<K>> , IEdge <K>
{
	private final K v;
	private final K w;
	private final double weight;

	public Edge(K v, K w, double weight)
	{
		if (Double.isNaN(weight)) throw new IllegalArgumentException("Weight is NaN");
		this.v = v;
		this.w = w;
		this.weight = weight;
	}

	public K either() {
		return v;
	}

	public K other(K vertice) {
		return (vertice.equals(v))? w:v;
	}

	public double weight() {
		return weight;
	}

	public String toString() {
		return v.toString() + " <--> " + w.toString() + " Peso:" + String.format("%5.2f", weight);
	}

	public NodoCamino<K> camino (int lenght, double peso){
		return new NodoCamino <K> (w , v, peso + weight, lenght);
	}

	@Override
	public int compareTo(Edge<K> arg) {
		if (weight == arg.weight)
			return 0;
		return (weight <= arg.weight)? -1:1;
	}

	@Override
	public K getNodoSalida() {
		// TODO Auto-generated method stub
		return w;
	}

	@Override
	public K setNodoSalida() {
		// TODO Auto-generated method stub
		return w;
	}

	@Override
	public K getNodoLlegada() {
		// TODO Auto-generated method stub
		return v;
	}

	@Override
	public K setNodoLlegada() {
		// TODO Auto-generated method stub
		return v;
	}


}
