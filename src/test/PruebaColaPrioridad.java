package test;

import data_structures.FixedHeap;

import data_structures.ListaEnlazadaSimple;
import data_structures.MaxHeapCP;
import data_structures.NodoSimple;
import junit.framework.TestCase;
import model.logic.GeneradorDatos;


public class PruebaColaPrioridad extends TestCase {

	private MaxHeapCP <PruebaVO> heap;
	private GeneradorDatos generadorDatos = new GeneradorDatos();

	private void setupEscenario1 (){
		heap = new MaxHeapCP <PruebaVO> (500);
	}

	public void test() {
		setupEscenario1 ();
		int repeticiones = 1;
		int n = 2200;
		while (n <= 5000)
		{
			for (int k = 0 ; k < repeticiones; k++)
			{
				Integer [] arregloEnteros = GeneradorDatos.generarAgnos(n);
				String [] arregloStrings = generadorDatos.generarCadenas(n);
				PruebaVO [] arregloObjetos = new PruebaVO [n];

				for (int i = 0 ; i < n ; i++){
					PruebaVO pruebaAgregar = new PruebaVO();
					pruebaAgregar.setAgno(arregloEnteros[i]);
					pruebaAgregar.setNombre(arregloStrings[i]);
					arregloObjetos[i] = pruebaAgregar;
				}
				for (int i = 0; i < n; i++)		
					heap.agregar(arregloObjetos[i]);

				assertEquals(n, heap.darNumElementos());

				ListaEnlazadaSimple <PruebaVO> listaOrdenada = heap.retornarOrdenado(true);
				NodoSimple <PruebaVO> actual = listaOrdenada.darPrimerNodo();
				while (actual.sig != null){
					assertTrue (actual.item.compareTo(actual.sig.item) <= 0);
					actual = actual.sig;
				}

				PruebaVO pruebaHeap = heap.max();

				for (int i = 1; i < n ; i++){
					PruebaVO pruebaHeap2 = heap.max();

					assertTrue(pruebaHeap.compareTo(pruebaHeap2) >= 0);
					//System.out.println(pruebaHeap.darNombre() + "-" + pruebaHeap.darAgno() + " >= " + pruebaHeap2.darNombre() + "-" + pruebaHeap2.darAgno());

					pruebaHeap = pruebaHeap2;
					assertEquals(n-i-1, heap.darNumElementos());
				}
			}
			n += 1000;
		}
		
	}
	
	
	public void test2(){
		int [] enteros = {45,70,34,69,26,85,24,64,54,98,32,23,75,86,22,65,87,19,42,53,11};
		String [] cadenas = {"fer","rfr","dsr","fhre","tddv","tfsvd","ytnf","asax","ntr","ybtd","ysc","oumj","fhhdhf","dfsc","zaxw","oijuy","bdrw","cse","bsdvxs","oyjfd","casw"};
		int n = 10;
		FixedHeap<Integer> heapInteger = new FixedHeap<>(n);
		FixedHeap<String> heapString = new FixedHeap<>(n);
		
		for (int i = 0; i < enteros.length; i++){
			heapInteger.agregar(enteros[i]);
			heapString.agregar(cadenas[i]);
		}
		assertEquals(n, heapInteger.darNumElementos());
		assertEquals(n, heapString.darNumElementos());
		
		ListaEnlazadaSimple<Integer> listaEnteros = heapInteger.retornarOrdenado(true);
		ListaEnlazadaSimple<String> listaCadenas = heapString.retornarOrdenado(true);
		for (String s : listaCadenas)
			System.out.println(s);
		for (Integer p : listaEnteros)
			System.out.println(p);
		
	}
}
