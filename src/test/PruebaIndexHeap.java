package test;

import data_structures.IndexMinHeap;
import junit.framework.TestCase;
import model.logic.GeneradorDatos;

public class PruebaIndexHeap extends TestCase {

	private IndexMinHeap <Integer, String> pq;
	private GeneradorDatos generadorDatos;
	
	private void setupEscenario1(){
		pq = new IndexMinHeap <Integer, String> (20);
	}
	private void setupEscenario2(int tamanio){
		pq = new IndexMinHeap <Integer, String> (tamanio);
		generadorDatos = new GeneradorDatos();
	}

	public void test(){
		int [] enteros = {45,70,34,69,26,85,24,64,54,98,32,23,75,86,22,65,87,19,42,53,11};
		String [] cadenas = {"fer","rfr","dsr","fhre","tddv","tfsvd","ytnf","asax","ntr","ybtd","ysc","oumj","fhhdhf","dfsc","zaxw","oijuy","bdrw","cse","bsdvxs","oyjfd","casw"};

		setupEscenario1();
		System.out.println(pq.tamanoMax());
		for (int i = 0; i < enteros.length; i++){
			pq.agregar(enteros[i], cadenas[i], false);
			assertEquals(i+1, pq.darNumElementos());
		}
		System.out.println(pq.tamanoMax());

		assertEquals(cadenas[0], pq.darValor(enteros[0]));
		pq.agregar(enteros[0], "aaaa", false);
		assertEquals("aaaa", pq.darValor(enteros[0]));
		System.out.println(pq.min());
	}

	public void testAleatorio (){
		int n = 2000;
		setupEscenario2((n/2)-1);

		Integer [] arregloEnteros = GeneradorDatos.generarNumeros(n);
		String [] arregloStrings = generadorDatos.generarCadenas(n);

		assertTrue(pq.esVacia());
		for (int i = 0; i < arregloEnteros.length; i++){
			pq.agregar(arregloEnteros[i], arregloStrings[i], false);
			assertEquals(i+1, pq.darNumElementos());
			assertTrue(pq.contiene(arregloEnteros[i]));
			assertEquals(arregloStrings[i], pq.darValor(arregloEnteros[i]));
		}
		assertEquals(((n/2)-1)*4, pq.tamanoMax());

		String [] arregloStrings2 = generadorDatos.generarCadenas(n);
		for (int i = 0; i< arregloEnteros.length; i+=2){
			pq.agregar(arregloEnteros[i], arregloStrings2[i], false);
			assertTrue(pq.contiene(arregloEnteros[i]));
			assertEquals(arregloStrings2[i], pq.darValor(arregloEnteros[i]));
		}

		String actual = null;
		for (String s : pq.retornarOrdenado(true)){
			if (actual!= null)
				assertTrue(actual.compareTo(s)<=0);
			actual = s;
		}
	}
}
