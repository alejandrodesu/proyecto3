package test;

public class PruebaVO implements Comparable <PruebaVO> {

	private String nombre;
	private int agno;
	
	public void setAgno (int anio)
	{
		this.agno = anio;
	}
	public int darAgno ()
	{
		return agno;
	}
	public void setNombre (String nom)
	{
		this.nombre = nom;
	}
	public String darNombre()
	{
		return nombre;
	}
	
	public int compareTo(PruebaVO arg0) 
	{
		if (agno == arg0.agno)
		{
			return nombre.compareTo(arg0.nombre);
		}
		return (agno < arg0.agno) ? -1: 1;
	}
}
