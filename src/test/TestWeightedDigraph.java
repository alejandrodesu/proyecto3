package test;

import java.util.Iterator;

import data_structures.NodoCamino;
import data_structures.WeightedGraph;
import junit.framework.TestCase;

public class TestWeightedDigraph extends TestCase{
	WeightedGraph<Integer, Integer> grafo;

	private void setupEscenario1() {
		grafo = new WeightedGraph<>(9);
	}

	public void test (){
		setupEscenario1();

		int [] verticesPrueba = {1,3,9,7,5,6,8,2,4};
		int [] grado = {1,2,1,3,4,3,1,2,1};
		int [] idFin = {5,1,5,1,9,2,1,7,2,4,6,4,8,3,3,9,4,8};
		double [] peso = {4,2,4,4,6,1,10,5,8,8,5,4,6,6,4,4,1,3};
		char [] orden = {'a', 'b', 'c', 'd'};


		for (int i = 0; i < verticesPrueba.length; i++)
			grafo.agregarVertice(verticesPrueba[i], verticesPrueba[i]);

		int k = 0;
		for (int i = 0; i < verticesPrueba.length; i++){
			for (int j = 0; j < grado[i]; j++){
				//grafo.agregarArco(verticesPrueba[i], idFin[k], peso [k], orden[j]);
				k++;
			}
		}

		assertEquals(verticesPrueba.length, grafo.numVertices());
		assertEquals(idFin.length, grafo.numArcos());
		/**
		for (int i = 0; i < grado.length; i ++)
			assertEquals(grado[i], grafo.darGrado(verticesPrueba[i]));
		 */

		int idOrigen = 9;
		int idDestino = 8;
		System.out.println("----DFS----");
		for (NodoCamino <Integer> camino : grafo.darCaminoDFS(idOrigen, idDestino))
			System.out.println(camino.toString());

		System.out.println("----BFS----");
		for (NodoCamino <Integer> camino : grafo.darCaminoBFS(idOrigen, idDestino))
			System.out.println(camino.toString());

		System.out.println("----dijkstra----");
		for (NodoCamino <Integer> camino : grafo.darCaminoDijkstra(idOrigen, idDestino))
			System.out.println(camino.toString());

		//Prim MST
//		for (int i = 0; i< verticesPrueba.length; i++){
//			double weigth = 0.0;
//			for (NodoCamino <Integer> camino : grafo.prim(verticesPrueba[i]))
//				weigth += camino.weight;
//			assertEquals(23.0, weigth, 0.0);
//		}

		assertEquals(1, grafo.componentesConectados());
		grafo.agregarVertice(32, 32);
		assertEquals(2, grafo.componentesConectados());

	}

	public void tedstCantidadesYVarios()
	{
		test();
		System.out.println("el numero de vertices es:  " +grafo.numVertices());
		assertEquals(9, grafo.numVertices());
		assertEquals(18, grafo.numArcos());
		//test de grados
		int [] grado = {1,2,1,3,4,3,1,2,1};
		int [] verticesPrueba = {1,3,9,7,5,6,8,2,4};
		/**
		for (int i = 0; i < 9 ;i++)
		{
			assertEquals(grado[i], grafo.darGrado(verticesPrueba[i]));

		}
		 */
		//test de dar arco
		System.out.println("dar peso del arco de 2 a 9 :  "+ grafo.darPesoArco(2, 9));
		assertEquals(4.0, grafo.darPesoArco(2, 9));
		//test vertices adyacentes e iterador de vertices
		Iterator iter = grafo.darVertices();
		int a = 0;
		while(iter.hasNext())
		{
			int actual =  (int) iter.next();
			System.out.println("el actual del iterador es :  "+ actual);
		}

	}
}
