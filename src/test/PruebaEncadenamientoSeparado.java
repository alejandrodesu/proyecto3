package test;

import java.io.FileWriter;
import java.io.PrintWriter;

import model.logic.GeneradorDatos;
import data_structures.EncadenamientoSeparadoTH;
import data_structures.ListaEnlazada;
import data_structures.Nodo;
import junit.framework.TestCase;

public class PruebaEncadenamientoSeparado extends TestCase
{
	private static final int CONSTANTE = 10;

	private EncadenamientoSeparadoTH<String, Integer> tablaHash;

	private void setupEscenario1()
	{
		tablaHash = new EncadenamientoSeparadoTH <>(CONSTANTE);
	}
	private void setupEscenario2(int tam)
	{
		tablaHash = new EncadenamientoSeparadoTH <>(tam);
	}

	public void tesstLavesDiferentes()
	{
		setupEscenario1();
		String [] cadenas = GeneradorDatos.generarCadenasPrueba(CONSTANTE);
		Integer enteros [] = GeneradorDatos.generarNumeros(CONSTANTE);

		assertTrue(tablaHash.estaVacia());
		for ( int i = 0; i < CONSTANTE; i++)
			tablaHash.insertar(cadenas[i], enteros[i]);

		assertFalse(tablaHash.estaVacia());
		assertEquals(tablaHash.darTamanio(), CONSTANTE);

		for (int i = 0; i < CONSTANTE; i++)
		{
			System.out.println(cadenas[i] + ": " + enteros[i] + " =? " + tablaHash.darValor(cadenas[i]));
			assertTrue(tablaHash.darValor(cadenas[i]) == enteros[i]);
		}
	}

	public void testEscenarios()
	{
		int N = 5;
		setupEscenario2(N);

		int [] enteros = {45,70,34,69,26,85,24,64,54,98,32,23,75,86,22,65,87,19,42,53,11};
		String [] cadenas = {"fer","rfr","dsr","fhre","tddv","tfsvd","ytnf","asax","ntr","ybtd","ysc","oumj","fhhdhf","dfsc","zaxw","oijuy","bdrw","cse","bsdvxs","oyjfd","casw"};

		assertTrue(tablaHash.estaVacia());
		for (int i = 0; i < enteros.length; i++)
		{
			assertEquals(i, tablaHash.darTamanio());
			tablaHash.insertar(cadenas[i], enteros[i]);
		}
		//Se hizo el rehash correctamente
		assertEquals(N*2, tablaHash.darCapacidad());
		assertEquals(enteros.length, tablaHash.darTamanio());

		//Verifica que se hayan insertado
		for (int i = 0; i < enteros.length; i++)
			assertTrue(tablaHash.darValor(cadenas[i]) == enteros[i]);

		//A�adir un valor con una llave existente
		int nAntes = tablaHash.darTamanio();
		tablaHash.insertar(cadenas[4], 92);
		assertEquals(nAntes, tablaHash.darTamanio());
		assertEquals(92, (int) tablaHash.darValor(cadenas[4]));

		//A�adir un valor null
		tablaHash.insertar(cadenas[5], null);
		assertEquals(nAntes - 1, tablaHash.darTamanio());
		assertFalse(tablaHash.tieneLlave(cadenas[5]));
		assertNull(tablaHash.darValor(cadenas[5]));
	}

	public void teestGeneraDatos()
	{
		int repeticiones = 0;
		for (int p = 0; p < repeticiones; p ++)
		{
			int [] Ns = {1000, 5000, 10000, 25000, 50000 , 100000, 250000, 500000, 1000000, 1250000, 1500000, 2000000};
			long [] tiempos = new long [Ns.length*1];
			int [] [] [] longitudes = new int [Ns.length] [10] [];
			for ( int j = 0; j < Ns.length; j++)
			{
				for (int k = 10; k <= 10; k+=10)
				{
					setupEscenario2(Ns[j]);
					//GeneradorDatos generador = new GeneradorDatos();
					String [] cadenas = GeneradorDatos.generarCadenas(Ns[j], k);
					Integer enteros [] = GeneradorDatos.generarNumeros(Ns[j]);

					assertTrue(tablaHash.estaVacia());

					long time = System.currentTimeMillis();
					for ( int i = 0; i < Ns[j]; i++)
						tablaHash.insertar(cadenas[i], enteros[i]);
					time = System.currentTimeMillis() - time;

					tiempos[((j)*1+k/10)-1] = time;
					int [] tamanios = tablaHash.darLongitudListas();
					longitudes [j][(k/10)-1] = tamanios;

					int cantidad = 0;
					for (int i =0; i < tamanios.length; i++)
						cantidad += tamanios[i];
					assertEquals(tablaHash.darTamanio(), cantidad);
					assertFalse(tablaHash.estaVacia());
				}
			}
			assertTrue (escritor (Ns, tiempos, longitudes));
		}
	}

	private boolean escritor (int[] Ns, long[] tiempos, int [] [] [] longitudes)
	{
		try
		{
			PrintWriter writer = new PrintWriter(new FileWriter("data/datos.txt", true));
			String encabezado = "N\t";
			for (int i = 10; i<= 10; i +=10)		
				encabezado += i + "\t";

			writer.println(encabezado);

			for ( int j = 0; j < Ns.length; j++)
			{
				String linea = Ns[j] + "\t";
				for (int k = 10; k <= 10; k+=10)
					linea += tiempos[((j)*1+k/10)-1] + "\t";
				writer.println(linea);
			}
			/**
			for ( int j = 0; j < Ns.length; j++)
			{
				writer.println(Ns[j]);		
				for (int k = 10; k <= 10; k+=10)
				{
					String linea = k + "\t";
					for (int i = 0; i < longitudes[j][(k/10)-1].length; i++)
						linea += longitudes[j][(k/10)-1][i] + "\t";
					writer.println(linea);
				}
			}*/
			writer.close();
		}
		catch (Exception e){
			return false;
		}
		return true;
	}
	
	public void testOrdenarListaEnlazada (){
		int [] enteros = {45,70,34,69,26,85,24,64,54,98,32,23,75,86,22,65,87,19,42,53,11};
		String [] cadenas = {"fer","rfr","dsr","fhre","tddv","tfsvd","ytnf","asax","ntr","ybtd","ysc","oumj","fhhdhf","dfsc","zaxw","oijuy","bdrw","cse","bsdvxs","oyjfd","casw"};
		
		ListaEnlazada <Integer, String > listaPrueba = new ListaEnlazada <Integer, String >();
		
		for (int i = 0; i < enteros.length; i ++)
			listaPrueba.agregarElementoFinal(enteros[i], cadenas[i]);
		
		assertEquals(enteros.length, listaPrueba.darNumeroElementos());
		listaPrueba.ordenar(true);
		assertEquals(enteros.length, listaPrueba.darNumeroElementos());
		
		Nodo <Integer, String > actual = listaPrueba.darPrimerNodo();
		while (actual.sig != null){
			assertTrue(actual.key <= actual.sig.key);
			System.out.println(actual.key + ": "+ actual.item);
			actual = actual.sig;
		}
	}
}
