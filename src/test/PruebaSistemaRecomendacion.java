package test;

import API.IEdge;
import API.ILista;
import API.SistemaRecomendacion;
import VOS.VOGeneroPelicula;
import VOS.VOPeliculaPlan;
import VOS.VOTeatro;
import VOS.VOUsuario;
import junit.framework.TestCase;

public class PruebaSistemaRecomendacion extends TestCase {
	public final static String RUTA_PELIS = "data/Proyecto 3/movies_filtered.json";
	public final static String RUTA_RAITING = "data/Proyecto 3/ratings_filtered.csv";
	public final static String RUTA_CARTELERA1 = "data/Proyecto 3/Programacion/dia1.json";
	public final static String RUTA_CARTELERA2= "data/Proyecto 3/Programacion/dia2.json";
	public final static String RUTA_CARTELERA3 = "data/Proyecto 3/Programacion/dia3.json";
	public final static String RUTA_CARTELERA4 = "data/Proyecto 3/Programacion/dia4.json";
	public final static String RUTA_CARTELERA5 = "data/Proyecto 3/Programacion/dia5.json";
	public final static String RUTA_TEAROS = "data/Proyecto 3/teatros_v4.json";
	public final static String RUTA_SIMILITUDES = "data/Proyecto 3/parteA/simMatriz.json";
	public final static String RUTA_RED = "data/Proyecto 3/tiempos1.json";


	private SistemaRecomendacion sr;

	private void setupEscenario1()
	{
		sr = new SistemaRecomendacion();
	}

	public void test()
	{
		setupEscenario1();
		assertTrue(sr.cargarPeliculas(RUTA_PELIS));
		assertTrue(sr.cargarRatingsSR(RUTA_RAITING));
		assertTrue(sr.cargarTeatros(RUTA_TEAROS));
		assertTrue(sr.cargarRed(RUTA_RED));
		assertTrue(sr.cargarSimilitudes(RUTA_SIMILITUDES));
		assertTrue(sr.cargarCartelera(RUTA_CARTELERA1,RUTA_CARTELERA2, RUTA_CARTELERA3, RUTA_CARTELERA4, RUTA_CARTELERA5 ));

	}
}
