package test;

import java.util.Iterator;

import model.logic.GeneradorDatos;
import data_structures.ListaEnlazada;
import data_structures.ListaEnlazadaSimple;
import data_structures.MaxHeapCP;
import data_structures.Nodo;
import data_structures.NodoSimple;
import data_structures.RedBlackBST;
import junit.framework.TestCase;

public class PruebaRedBlackBST extends TestCase{

	private RedBlackBST<String , Integer> arbol;
	
	private void setupEscenario1()
	{
		arbol = new RedBlackBST<String , Integer>();
	}
	public void test1()
	{
		setupEscenario1();
		
		int n = 160783;
		GeneradorDatos generador = new GeneradorDatos();
		String [] cadenas = generador.generarCadenas(n);
		Integer [] enteros = GeneradorDatos.generarNumeros(n);
		
		assertTrue(arbol.isEmpty());
		for (int i = 0; i < n; i ++)
			arbol.put(cadenas[i], enteros[i]);
		assertFalse(arbol.isEmpty()); assertEquals(n, arbol.size());
		
		//Insert� bien
		for (int i = 0; i < n; i ++)
		{
			try {
				assertEquals(enteros[i], arbol.get(cadenas[i]));
			}catch (Exception e){
				fail();
			}
		}
			
		
		//Probar recorrido inorden
		ListaEnlazada <String , Integer> inorden = arbol.inorden(true);
		Iterator <String>  iter = inorden.iteratorKeys();
		String anterior = iter.next();
		while(iter.hasNext()){
			String actual = iter.next();
			assertTrue(anterior.compareTo(actual) < 0);
			anterior = actual;
		}
		assertEquals(n, inorden.darNumeroElementos());
		
		/**
		String [] cadenasOrdenadas = new String[n];
		int indice = 0;
		for (String key: inorden){
			cadenasOrdenadas [indice] = key;
			indice++;
		}*/
	
		//Ordenar cadenas usando HeapSort
		MaxHeapCP <String> heap = new MaxHeapCP <String>(cadenas);
		ListaEnlazadaSimple <String> cadenasOrdenadas = heap.retornarOrdenado(true);
		Integer [] enterosOrdenados = new Integer [n];
		assertEquals(n, cadenasOrdenadas.darNumeroElementos());
		
		//Verificar orden
		anterior = null;
		int indice = 0;
		for (String key: cadenasOrdenadas){
			if (anterior != null)
				assertTrue(anterior.compareTo(key) < 0);
			anterior = key;
			try {
				enterosOrdenados [indice] = arbol.get(key);
			}catch (Exception e){
				fail();
			}
			indice++;
		}
	
		/**
		//Ubica los enteros en el orden correspondiente a la lista de cadenas ordenadas
		Integer [] enterosOrdenados = new Integer [n];
		for (int i = 0; i < n; i ++)
			enterosOrdenados [i] = arbol.get(cadenasOrdenadas[i]);
		*/
		
		//Prueba m�todo darLlavesValoresEnRango
		int min = n/3, max = n-4;
		ListaEnlazada <String , Integer> listaRango = arbol.darLlavesValoresEnRango(cadenasOrdenadas.darElemento(min), cadenasOrdenadas.darElemento(max));
		assertEquals(max-min+1, listaRango.darNumeroElementos());
		
		Nodo <String , Integer> actual = listaRango.darPrimerNodo();
		NodoSimple <String> actualS = cadenasOrdenadas.darPrimerNodo();
		for (int i = 1; i < min; i++)
			actualS = actualS.sig;
		while (actual != null && min <= max)
		{
			assertEquals(actual.key, actualS.item);
			assertEquals(actual.item, enterosOrdenados[min-1]);
			min++ ;
			actual = actual.sig;
			actualS = actualS.sig;
		}
	}
}
